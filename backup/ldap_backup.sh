#! /bin/sh

. /etc/alternc/alternc.conf

ldapsearch -L -L -L -x -s sub -D "cn=root,dc=system,dc=bureau,dc=koumbit,dc=net" -w $ldap_rootpwd -b "dc=postfix,dc=bureau,dc=koumbit,dc=net" "objectclass=*"
