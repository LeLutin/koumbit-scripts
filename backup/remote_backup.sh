#! /bin/sh

usage() {
	cat <<EOF
Usage: $0 [ -c N ] target file ...
  -c N: the number of backups to keep (defaults to 1)
  target: where to put the files on the local filesystem
  file: rsync-style paths to fetch from remote server(s)

The backups are rotated using cp -al, so that minimum disk space is used. See
[1] for details.

Example usage:

  $0 /backup/daily user@example.com:/var

will backup the /var partition on the example.com machine to
/backups/daily.0/var

  $0 /backup/daily user@example.com:/var

The same as above, except that the 'daily' directory is rotated using
cp -al. The backup will end up in /backups/daily.0/root and
/backups/daily.0/usr_home.

[1]: http://www.mikerubel.org/computers/rsync_snapshots/ 
EOF

}

# parse command line
args=`getopt c: $*`

if [ $? != 0 ]
then
    usage
    exit 2
fi
set -- $args

for i
do
  case "$i"
  in
      -c)
          count="$2"; shift;
          shift;;
      -h)
          usage;
          exit;;
      --)
          shift; break;;
  esac
done

if [ $# -lt 2 ]
then
    usage
    exit 2
fi

target="$1"
shift
files="$*"

# the number of backups to keep, defaults to 1
count=${count-1}

# dependencies
MV="mv -f"
# this needs the -a and -l flags, which GNU cp has
CP="cp -al"
EXPR="expr"
SED="sed"
# -a, --archive               archive mode, equivalent to -rlptgoD
# -x, --one-file-system       don�t cross filesystem boundaries
# -z, --compress              compress file data
# --delete                delete files that don�t exist on the sending side
# --delete-excluded       also delete excluded files on the receiving side
RSYNC="rsync -v -xaz --delete --delete-excluded"
SSH="ssh -T"

# no config below this line

# compat
remote_dir=$target

# more than one backup, create a batch command
if [ $count -gt 1 ]
then

	last=`$EXPR $count - 1`

        # keep last backup, it will be where we will be syncing
	$MV $remote_dir.$last $remote_dir.tmp 2>/dev/null
	while [ $last -gt 0 ]
	do
		from=`$EXPR $last - 1`
                # each directory between is pushed below
		$MV $remote_dir.$from $remote_dir.$last 2>/dev/null
		last=$from
	done
        # the last command:
        # - move the last backup to the current backup
        # - sync the current backup with it
        # we are now ready to sync
	$MV $remote_dir.tmp $remote_dir.0 2>/dev/null
        # -d, --make-directories
        #       Create leading directories where needed.
        # -l, --link
        #        Link files instead of copying them, when possible  (usable  only
        #        with the -p option).
        # -m, --preserve-modification-time
        #        Retain previous file modification times when creating files.
        # -p, --pass-through
        #        Run in copy-pass mode.
	cd $remote_dir.1 2>/dev/null && find . -print | cpio --quiet -mdpl $remote_dir.0 2>&1 | grep -v "not created: newer or same age version exists$"
fi

remote_dir=$remote_dir.0
$RSYNC $files $remote_dir | tail -2
