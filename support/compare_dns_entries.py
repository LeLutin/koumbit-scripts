#!/usr/bin/env python3
#
# Requires: dnspython
#  to install: apt install python3-dnspython
#
# Use for verifying the output of a nameserver against a known list.
#
# Example:
#  1. Create a file with a series of lines for each entry, separated by commas
#   eg. example.ca,A,192.168.1.1
#  2. Call this script:
#   eg. ./compare_dns_entries.py -f ~/example.txt ns0.koumbit.net
# Successes will not be output unless verbose is set, but failures with their
# differences will be output.
#
import argparse
import difflib
import socket
import sys

import dns.resolver

def main(nameservers, source_file = sys.stdin, verbose = False):
    with open(source_file, 'r', encoding='utf-8') as f:
        line = f.readline()
        resolver = dns.resolver.Resolver()
        resolver.nameservers = [socket.gethostbyname(ns) for ns in nameservers]
        failed = 0
        count = 0
        differ = difflib.Differ()
        while line:
            name, dns_type, expected_result = line.split(',')
            expected_result = expected_result.strip("\n")
            result, match = check_entry(name, expected_result, dns_type.upper(), resolver)
            count += 1
            if verbose and result:
                print("Check {} {} passed with matching result '{}'".format(
                    name, dns_type.upper(), expected_result))
            if not result:
                diff = differ.compare([expected_result + "\n"], [match + "\n"])
                print("Check {} {} failed: \n{}".format(
                    name, dns_type, ''.join(diff)))
                failed += 1
            line = f.readline()
        if verbose:
            print('Passed {}/{} checks'.format((count - failed), count))

def check_entry(name, expected_result, dns_type = 'A', resolver = None):
    if resolver is None:
        resolver = dns.resolver.Resolver()
    s = 'No answer'
    for rdata in resolver.query(name, dns_type, raise_on_no_answer = False):
        s = rdata.to_text()
        if s == expected_result:
            return (True, s)
    return (False, s)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('-f', '--file', default=sys.stdin,
                        help='The file to read defintions from, defaults to stdin')
    parser.add_argument('ns', nargs='+', help='The nameserver to test against')
    args = parser.parse_args()
    main(args.ns, args.file, args.verbose)
