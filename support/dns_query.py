#!/usr/bin/python3
#
#
"""DNS querying library.

This library provides helper functions to get information from DNS servers.

Functions in this library expect "res" to be an instance of
dns.resolver.Resolver from dnspython.
"""


class DomainError(Exception):
    """The domain name does not exist."""

    pass


# TODO: remove this once buster is not used by anyone anymore (e.g. once
# bullseye is out, we should push folks to upgrade!)
def dns_get_record(res, *params):
    """Resolve one DNS record.

    This is a shim function that keeps compatibility for 1.x versions of the
    dnspython library, and implements 2.x support.
    """
    if hasattr(res, "resolve"):
        return res.resolve(*params)
    else:
        # 1.x method
        return res.query(*params)
