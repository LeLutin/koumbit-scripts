#!/bin/bash
#
# Automatically answer to copyright infringement RT tickets
#
# This script can be found at https://wiki.koumbit.net/ComitéLégal/CopyrightInfringementNotices
#

# Space-separated list of copyright infringement notification sender emails
# it's always the same couple few...
INFRINGEMENT_EMAILS='antipiracy@p2p.markmonitor.com noreply@p2p.copyright-notice.com p2p@copyright.ip-echelon.com notice@hbo.copyright-notice.com %@p2p.opsecsecurity.com p2p@viacomcbs.copyright-notice.com p2p@warner.copyright-notice.com'

function usage()
{
  echo "usage: $0 -c <CLIENT_EMAIL> [<RT_TICKET_ID>...]" >&2
  echo >&2
  echo "Automatically reply to copyright infringement RT tickets." >&2
  echo "Multiple ticket IDs can be specified if they all pertain to the same client." >&2
  echo "Client email must be found in accounting software after identifying which client is behind" >&2
  echo "the IP targetted by the copyright claim." >&2
  echo >&2

  exit 1
}

CLIENT_EMAIL=''
RT_TICKET=''

while [ $# -gt 0 ] ; do
  case "$1" in
    -c) CLIENT_EMAIL="$2"; shift;;
    -*) usage;;
    *) RT_TICKET="$1 $RT_TICKET";;
  esac
  shift
done

if [ -z "$CLIENT_EMAIL" ] ; then
  usage
fi

if [ -z "$RT_TICKET" ] ; then
  email_filter=
  for e in $INFRINGEMENT_EMAILS; do
    if [ -z "$email_filter" ]; then
      email_filter="Requestor.EmailAddress like '${e}'"
    else
      email_filter="${email_filter} or Requestor.EmailAddress like '${e}'"
    fi
  done
  ticket_query="(Status = 'new' or Status = 'open') and (${email_filter})"

  # Get all ticket numbers
  # We're getting rid of the type prefix so that we can reuse the same
  # commands than if ticket numbers are supplied.
  RT_TICKET=$(rt ls -t ticket -i -q support "$ticket_query" | sed 's#ticket/##g')
  # Print out a more verbose list so that users can confirm that they are all
  # copyright infringement emails
  echo -e "\\e[36mThe following tickets were found and will be processed\\e[0m:"
  echo
  rt ls -t ticket -q support "$ticket_query"
  echo
  read -r -p "Proceed with the above list? [Y/n] " proceed
  if [[ ! $proceed =~ ^(y|Y|)$ ]]; then
    echo aborting
    exit
  fi
fi

CLIENT_MESSAGE=$'We are forwarding you this notice. (We are legally required to forward you such notices).\nKoumbit'
PLAIGNANT_MESSAGE=$'To whom it may concern,\n\nPlease note that your message has been transferred to the client responsible for this server.\n\nSincerely,\nKoumbit'
for i in $RT_TICKET; do
  ORIGINAL=$(rt show "ticket/$i/history/Create")
  FULL_MESSAGE=$(printf "%s\n\n-------- Original Message --------\n\n\n%s" "$CLIENT_MESSAGE" "$ORIGINAL")
  rt comment -c "$CLIENT_EMAIL" -m "$FULL_MESSAGE" "ticket/$i"
  rt correspond -m "$PLAIGNANT_MESSAGE" "ticket/$i"
  rt edit "ticket/$i" set status=resolved queue=Legal
done
