#!/bin/bash
#
# To run this script, make sure to configure your local rt CLI
# See https://wiki.koumbit.net/RequestTrackerConfiguration#CLI
#
# You also need to install GNU Parallel: apt install parallel
#
rt ls -i -q roots "Status=new and LastUpdated < '3 weeks ago'" | parallel --progress --pipe -N50 -j1  -v --halt 1 rt edit - set status=deleted

