#!/usr/bin/env bash

RISEUP_IP_REGEX="199.58.83.9\|199.58.83.11"
RT_SUPPORT_QUERY="( Queue = 'CSN-MonSyndicat' OR Queue = 'Comptes' OR Queue = 'Info' OR Queue = 'Support' OR Queue = 'Ventes' OR Queue = 'Production' ) AND (  Status = 'new' OR Status = 'open' ) AND Owner = 'Nobody'"
SCRIPT_DIR=$(dirname "$0")

# list support tickets
# search for Infringement
ticket_numbers=($(rt ls -t tickets -q support -s "$RT_SUPPORT_QUERY" \
                    | grep -i "infringement\|unauthorized\|Digital Millennium Copyright Notice" \
                    | cut -d ":" -f1))
riseup_tickets=()
other_tickets=()

if [[ 0 -eq ${#ticket_numbers[*]} ]] ; then
    printf "[+] No copyright infringement tickets have been found.\n"
    exit 0
else
    printf "[+] Found ${#ticket_numbers[*]} copyright infringement tickets.\n"
fi

for ticket_id in ${ticket_numbers[*]} ; do
    if [[ 0 -eq $(rt show ${ticket_id} | grep -q "$RISEUP_IP_REGEX") ]] ; then
        riseup_tickets+=($ticket_id)
    else
        other_tickets+=($ticket_id)
    fi
done

if [[ 0 -ne ${#riseup_tickets[*]} ]] ; then
    printf "[+] The following tickets belong to riseup: ${riseup_tickets[*]}\n"
    "${SCRIPT_DIR}"/infringement-notify.sh -c abuse@riseup.net "${riseup_tickets[*]}"
fi

if [[ 0 -ne ${#other_tickets[*]} ]] ; then
    printf "[+] The following tickets need an owner: ${other_tickets[*]}\n"
    printf "[+] Please search for the people behind flagged IP addresses.\n"
fi
