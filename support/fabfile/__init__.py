#!/usr/bin/env python3

from fabric import Connection
from fabric import task

import getpass
import os.path
import tempfile

@task
def check_release(c):
    c.run('lsb_release -a')

@task
def get_alternc_cert_id(c, domain = None, db = 'alternc'):
    if domain is None:
        domain = input("Enter the FQDN: ")
    sudo_pass = getpass.getpass("What's your sudo password for '{}'?".format(c.host))
    c._config['sudo']['password'] = sudo_pass
    c.sudo("mysql alternc -e \"select id, uid, fqdn, altnames, validstart, validend from certificates where fqdn = '{}' and status = 1 \\G\"".format(domain))

@task
def copy_alternc_certificates_from(c, cert_id, source_host, dest_dir, dest_chain, dest_cert, dest_key, source_dir = '/var/lib/alternc/ssl/private/0', ):
    """
    cert_id: the alternc certificate ID.
    source_host: eg. alternc.example.com
    dest_dir: eg. /var/lib/ssl/custom/
    dest_chain: the name of the chain file on the destination, relative to dest_dir
    dest_cert: the name of the cert file on the destination, relative to dest_dir
    dest_key: the name of the key file on the destionation, relative to the dest_dir
    source_dir: where alternc is stocking it's certs
    """
    # c is the destination connection
    sudo_pass = getpass.getpass("What's your sudo password for '{}'?".format(c.host))
    c._config['sudo']['password'] = sudo_pass

    # These are removed when the scoped is closed
    local_cert = tempfile.NamedTemporaryFile()
    local_chain = tempfile.NamedTemporaryFile()
    local_key = tempfile.NamedTemporaryFile()

    # This is hot garbage, because put/get don't support using sudo.
    # As a result this doesn't work at all.
    with Connection(source_host) as src:
        sudo_pass = getpass.getpass("What's your sudo password for '{}'?".format(src.host))
        src._config['sudo']['password'] = sudo_pass
        src.get(os.path.join(source_dir, "{}.pem".format(cert_id)),
                local_cert.name)
        src.get(os.path.join(source_dir, "{}.key".format(cert_id)),
                local_key.name)
        src.get(os.path.join(source_dir, "{}.chain".format(cert_id)),
                local_chain.name)
    c.put(local_cert.name, os.path.join(dest_dir, dest_cert))
    c.put(local_key.name, os.path.join(dest_dir, dest_key))
    c.put(local_chain.name, os.path.join(dest_dir, dest_chain))

@task
def reload_apache(c):
    sudo_pass = getpass.getpass("What's your sudo password for '{}'?".format(c.host))
    c._config['sudo']['password'] = sudo_pass
    if not c.sudo('apache2ctl configtest').failed:
        c.sudo('service apache2 graceful')

@task
def list_disks(c):
    c.run('lsblk -d -o +MODEL,REV,SERIAL')

@task
def hdinfo(c, disk):
    sudo_pass = getpass.getpass("What's your sudo password for '{}'?".format(c.host))
    c._config['sudo']['password'] = sudo_pass
    c.sudo('hdparm -I {}'.format(disk))

