#!/usr/bin/python3
"""Merge one or more RT tickets into a destination one.

Usage:
  merge-into.py <destination_ticket_id> [<ticket_id> ...]

Options:
  -h, --help: print this help text

If no ticket number is given, the list of currently open tickets will be
displayed and you will be able to interactively choose which ones to mark as
spam.
"""

import sys


from support_rt.connect import connect_to_rt
from support_rt.tickets import get_support_tickets, choose_tickets


if __name__ == '__main__':
    if "-h" in sys.argv or "--help" in sys.argv:
        print(__doc__)
        exit(0)

    if len(sys.argv) < 2:
        print(__doc__)
        print("error: please supply at least the destination ticket id",
              file=sys.stderr)
        exit(1)

    try:
        destination = int(sys.argv[1])
        tickets = [int(x) for x in sys.argv[2:]]
    except ValueError:
        print(__doc__)
        print("error: All arguments should be integer numbers",
              file=sys.stderr)
        exit(1)

    try:
        tracker = connect_to_rt()
    except Exception:
        print("error: Failed to connect to RT")
        exit(1)

    if not len(tickets):
        qprompt = "Which tickets need to be merged into {}?".format(
                    destination)
        tickets = choose_tickets(get_support_tickets(tracker), message=qprompt)

    # TODO: show summary of what will be done and ask for confirmation

    for ticket in tickets:
        print("Merging ticket {} into destination".format(ticket))
        tracker.ticket.merge(ticket, destination)
