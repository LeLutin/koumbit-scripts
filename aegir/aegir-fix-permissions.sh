#!/bin/bash

# Sets permissions and ACLs on aegir site.

# Set defaults
FORCE=false # If set, will continue without the given group existing on the system
SFTP=false # Repaires the permissions in symlinked directories (eg. sites with our in-house sftp linked)
SFTP_USER='' # User for the sftp account
SITE_PATH='' # Absolute site path
GROUP='' # the client's group, eg. cl-sampleclient

# Parse args: http://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
#echo "Num args: $#"
while [[ $# -ge 1 ]]; do
  key="$1"
  #echo "key: $key"
  case $key in
    -s|--sftp)
      SFTP="true"
      shift
      SFTP_USER="$1"
      ;;
    -f|--force)
      FORCE="true"
      ;;
    *)
      if [[ "$SITE_PATH" = '' ]]; then
        SITE_PATH=$key
        shift
        continue
      fi
      if [[ "$GROUP" = '' ]]; then
        GROUP=$key
        shift
        continue
      fi
      ;;
  esac
  shift
done

usage() {
  echo "$0 [-s|--sftp USER] [-f|--force] SITE_PATH GROUP"
  echo -e "\tSITE_PATH: absolute path to site, eg. /var/aegir/platforms/drupal-7.43/sites/myblog.xyz"
  echo -e "\tGROUP: the internal group name for the client, eg. cl-sampleclient"
  echo -e "\t[-s USER|--sftp USER]: fix permissions inside directories that are commonly symlinked for sftp accounts. USER is sftp account username, eg. client-sftponly"
  echo -e "\t[-f|--force]: continue even when the group passed as an argument does not exist"
}


if ! id -u > /dev/null; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

#echo "SFTP_USER: $SFTP_USER"
#echo "FORCE: $FORCE"
#echo "SFTP: $SFTP"
#echo "Path: $SITE_PATH"
if [[ "$SITE_PATH" = '' ]]; then
   echo "path argument not set"
   usage
   exit 255
fi

#echo "Group: $GROUP"
if [[ "$GROUP" = '' ]]; then
   echo "group argument not set"
   usage
   exit 255
fi

# Check to make sure group exists
if ! getent group $GROUP 1>/dev/null; then
   echo "Group $GROUP does not exist."
   if [[ $FORCE != "true" ]]; then
     usage
     exit 1
   fi
fi

if ! id $SFTP_USER 2>&1 2>&1 > /dev/null && [[ "$SFTP" = "true" ]]; then
   echo "User $SFTP_USER does not exist."
   usage
   exit 1
fi

PARTS=("modules" "themes" "libraries" "files" "private")
# Change ownership, special case for several SFTP is enabled.
for part in ${PARTS[@]}; do
   chown -R aegir:www-data $SITE_PATH/$part
   chmod -R ug+rw,g+s,a+r $SITE_PATH/$part
   if [ $SFTP = "true" ]; then
      chown -R $SFTP_USER:www-data $SITE_PATH/$part/.
      chmod -R ug+rw,g+s,a+r $SITE_PATH/$part/.
   fi
done

setfacl -R -m user:aegir:rwx $SITE_PATH
setfacl -R -m default:user:aegir:rwx $SITE_PATH
setfacl -R -m group:www-data:rwx $SITE_PATH
setfacl -R -m d:group:www-data:rwx $SITE_PATH
setfacl -R -m group:$GROUP:rwx $SITE_PATH
setfacl -R -m default:group:$GROUP:rwx $SITE_PATH
if [ $SFTP = "true" ] && [ $SFTP_USER != '' ]; then
   for part in ${PARTS[@]}; do
      p=$SITE_PATH/$part/.
      setfacl -R -m user:$SFTP_USER:rwx $p
      setfacl -R -m user:aegir:rxw $p
      setfacl -R -m default:user:aegir:rxw $p
      setfacl -R -m group:www-data:rwx $p
      setfacl -R -m d:group:www-data:rwx $p
      setfacl -R -m group:$GROUP:rwx $p
      setfacl -R -m default:group:$GROUP:rwx $p
   done
fi

chown 755 $SITE_PATH/.
chown aegir:www-data $SITE_PATH/settings.php
chmod 440 $SITE_PATH/settings.php
chown aegir:www-data $SITE_PATH/local.settings.php
chmod 640 $SITE_PATH/local.settings.php
