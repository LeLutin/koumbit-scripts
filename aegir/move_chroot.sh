#!/bin/bash

#
# move_chroot.sh
#
# This moves all the required parts of a drupal site to a client home dir and
# make the proper symlink in the aegir platform so subsequent don't fuck it
# up. This can (should?) be reverted by unmove_chroot.sh.
#
# Bugs: it doesn't check if the site is already in the user account. Currently
# it is "working", as if one of the parts was created after the move (the 
# private directory, which should maybe be created on the first pass of this
# script), it will not overwrite the old stuff, and move what's missing, but
# it might be sketchy.

domain="$1"
user="$2"
group="$3"

usage() {
  echo Takes three args: 1\) domain, 2\) user and 3\) group. Fourth and optional is platform.
}

if [ $# -lt 3 ] ; then
  usage
  exit 1
fi

if ! getent group $group ; then
  echo this group doesn\'t exist, please create it !
  exit 6
fi

if ! id $user &> /dev/null ; then
  echo this user \($user\) doesn\'t exist, please try again ;
  exit 2
fi

if test $(groups $user | cut -d ':' -f 2 | grep $group | wc -l) = 0 ; then
  echo Sorry, the user specified \($user\) is not in the group specified \($group\), so this
  echo script won\'t work. Please fix and try again.
  exit 4
fi

if [ $# -lt 4 ] ; then
  full_path=$(aegircd "$domain")
  if [ $? == 1 -o $? == 2 ] ; then
    echo could not find one and only one platform for $domain , specify it as a third argument...
    echo eg: run find /var/aegir/platforms/ -iname \"$domain\" for yourself and add the platform directory as a fourth argument
    exit 5
  fi

  # pretty ugly but works
  platform=$(dirname $(dirname "$full_path"))
else
  platform="$4"
fi

source="$platform/sites/$domain"
target="/home/$user/$domain"

if [[ ! -d $source ]] ; then
  # can't access source
  echo Can\'t access source \($source\) directory, probably you wrote the domain wrong
  exit 3
fi

# we create the directory for the user
if [[ ! -d $target ]] ; then
  echo That directory doesn\'t exists yet, or user doesn\'t have a homedir, creating $target
  mkdir -p "$target"
  chown "$user" "$target"
  chmod g+s "$target"
fi

# we move the data from the site in the home dir, and make links to the drupal tree
for part in modules files themes libraries private; do
  if [ -e $target/$part ] ; then
    echo target $target/$part already exists
  else
    mv $source/$part $target/$part && ln -s $target/$part $source/$part
    chown -R "$user" "$target/$part"
    setfacl -R -m group:$group:rwx "$target/$part"
    setfacl -R -m default:group:$group:rwx "$target/$part"
  fi
done
