#!/usr/bin/env python3
import datetime
import sys

def xth_of_month(day, week, month = None, year = None):
    """
    Returns the day number (0-31) of the Xth NDay of the Month.
    day is the iso day number (1 is Monday, 7 is sundy)
    week is an int, 1-4
    month and year are used to figure out when we're looking.
    defaults to current month and year.
    Eg. 3rd Monday is xth_of_month(1, 3)
    """
    day = int(day)
    week = int(week)
    if day < 1 or day > 7:
        # would loop forever checking for isoweekday
        return None
    today = datetime.datetime.today()
    if month is None:
        month = today.month
    if year is None:
        year = today.year
    cur_day = 1
    d = datetime.datetime(year, month, cur_day)
    while d.isoweekday() != day:
        cur_day += 1
        d = datetime.datetime(year, month, cur_day)
    # d should now have the first Nday of the month
    if week == 1:
        return d.day
    delta = datetime.timedelta(7*(week-1))
    d = d + delta
    if d.month != month:
        return None
    return d.day

if __name__ == '__main__':
    r = xth_of_month(sys.argv[1], sys.argv[2])
    if r is None:
        exit(-1)
    print(r)
    exit(0)
    #print('Argv:', sys.argv)
    #print('Xth day of Month:', xth_of_month(sys.argv[1], sys.argv[2]))
