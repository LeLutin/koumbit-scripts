#!/usr/bin/env python3
#
# To run this script, you need to: apt install python3-pymysql
#
"""Import contacts from another roundcube into the local one.

This script connects to a local database import of the roundcube database
from the source server.

So before running this script, you'll need to dump the roundcube database from
the source server and then import it to a new database (watch out not to
destroy the roundcube database that already exists on the destination server!).

You'll also need to create a user for this new database and grant access to the
import database as well as the production roundcube database.

A suggestion for naming the database that contains the roundcube import is to
name it after the redmine ticket you're using for this work. E.g.
'redmine12345678'. The user that has access to this database could for example
have the same name as the database.

The script currently assumes that the production roundcube database is called
'roundcube'.

The password that will be used for database connection will be prompted from the
terminal.
"""

import argparse
import getpass

import pymysql


def sync_users(old_db, new_db, COMMIT=True):
    old_users = get_users(old_db)
    new_users = get_users(new_db)
    users_to_migrate = sorted(old_users.difference(new_users))
    print("{} old users, {} new users, {} users to migrate".format(
        len(old_users), len(new_users), len(users_to_migrate)))
    for user in users_to_migrate:
        sync_user(user, old_db, new_db, COMMIT)

def sync_user(username, old_db, new_db, COMMIT):
    user_data = {}
    with old_db.cursor() as cursor:
        cursor.execute("select username, mail_host, created, last_login, language, preferences from users where mail_host='localhost' and username=%s", (username,))
        user_data = cursor.fetchall()[0]
    with new_db.cursor() as cursor:
        sql = "insert into users (username, mail_host, created, last_login, language, preferences) VALUES (%s, %s, %s, %s, %s, %s)"
        cursor.execute(sql, user_data)
        # Commit changes?
        if COMMIT:
            new_db.commit()

def sync_contacts(old_db, new_db, COMMIT=True):
    odb_cursor = old_db.cursor()
    odb_cursor.execute("select contacts.user_id, username, count(0) as count from contacts left join users on users.user_id = contacts.user_id group by user_id order by count asc")
    results = odb_cursor.fetchall()
    ndb_cursor = new_db.cursor()
    for r in results:
        print(r)
        new_id = get_userid(r[1], new_db)
        print(new_id)
        # Our heuristic to see if we've done the migration of contacts is
        # if there are as many or more contacts on new_db for the user than
        # in old_db
        ndb_cursor.execute("select count(0) from contacts where user_id = %s", (new_id,))
        new_contact_count = ndb_cursor.fetchall()[0][0]
        if new_contact_count >= r[2]:
            print("Skipping contacts for '{}', equal or more on new side".format(r[1]))
            continue
        odb_cursor.execute("select changed, del, name, email, firstname, surname, vcard, words from contacts where user_id=%s", (r[0],))
        old_contacts = odb_cursor.fetchall()
        for old_contact in old_contacts:
            contact = old_contact + (new_id,)
            ndb_cursor.execute("insert into contacts (changed, del, name, email, firstname, surname, vcard, words, user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    contact)
        if COMMIT:
            new_db.commit()
    ndb_cursor.close()
    odb_cursor.close()

def get_userid(username, db):
    with db.cursor() as cursor:
        cursor.execute("select user_id from users where username=%s", (username,))
        return cursor.fetchone()[0]

def get_users(db):
    with db.cursor() as cursor:
        cursor.execute("select username from users where mail_host='localhost' order by username desc")
        result = cursor.fetchall()
        result = [x[0] for x in result]
        return set(result)

if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument("dbname", help="Name of the local import database")
    argparser.add_argument("dbuser", help="User name used to connect to import database. Password will be prompted")
    argparser.add_argument("--no-commit", dest="commit", action='store_false',
                           help="Avoid committing changes to the database, run a dry run")

    args = argparser.parse_args()

    dbpassword = getpass.getpass("Database user password: ")

    print(f"dbname: {args.dbname}, dbuser: {args.dbuser}, pass: {dbpassword}, commit: {args.commit}")

    old_db = pymysql.connect(
        host='localhost',
        user=args.dbuser,
        password=dbpassword,
        database=args.dbname
    )
    new_db = pymysql.connect(
        host='localhost',
        user=args.dbuser,
        password=dbpassword,
        database='roundcube'
    )

    sync_users(old_db, new_db, COMMIT=args.commit)
    #sync_contacts(old_db, new_db, COMMIT=args.commit)
    old_db.close()
    new_db.close()
