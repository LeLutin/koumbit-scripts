#!/usr/bin/env python
# -*- coding: utf8 -*-

# Copyright 2012 Sofian Benaissa (sofian@koumbit.org).
# 
# This file is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any later 
# version. 
# 
# This file is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
# details. You can consult the license at http://www.gnu.org/licenses/


import optparse
import re

parser = optparse.OptionParser(
  usage="""mail-scanner.py <input file> <log file>

Given a file containing a list of emails, will scan the target log file and
output any and all lines pertaining to each email. This script must be run by a
user with read permissions on the target log file.

    <input file> should be a plain text file containing a list of email 
addresses, one address per line

    <log file> should be /var/log/mail.log or a similar system mail log file
"""
)

(options, args) = parser.parse_args()


mails = open(args[0], 'r')
log = open(args[1], 'r')
found = "Found references to these emails:\n"
notfound = "Did not find references to these emails:\n"
mailSent = "These mails were sent (status=sent in log line):\n"
mailBounced = "These mails bounced (status=bounced in log line):\n"
counts = {'sent':0, 'bounced':0, 'logged':0, 'absent':0}
details = "\nDetails\n-------\n"

for email in mails:
  email = email.strip()
  details += "\nSearching for: " + email
  status = []
  log.seek(1)
  for num, line in enumerate(log, 1):
    if line.find(email) > -1:
      details += "\n" + str(num) + ': ' + line
      status += ['logged']
      if line.find('status=') > -1:
        m = re.search('status=([a-zA-Z]+)', line)
        status += [m.group(1)]
  if 'logged' in status:
    found += '* ' + email + "\n"
    counts['logged'] += 1
  else:
    notfound += '* ' + email + "\n"
    counts['absent'] += 1

  if 'sent' in status:
    mailSent += '* ' + email + "\n"
    counts['bounced'] += 1
  if 'bounced' in status:
    mailBounced += '* ' + email + "\n"
    counts['sent'] += 1
      
mails.close()
log.close()
print "\nStatistics\n----------\n"
print "    %(logged)s found\n    %(absent)s not found\n    %(sent)s sent\n    %(bounced)s bounced" % counts
print "\nSummary\n-------\n"
print mailBounced
print mailSent
print notfound
print found
print details
