#!/bin/bash
#
# Usage avec téléchargement directe du wiki
# (one-time setup)
#   mkdir -p ~/.config/koumbit/
#   echo -e "FROM=user@example.com\nSMTP_USER=user@example.com\n" > ~/.config/koumbit/wiki_tasks_to_redmine.conf
#   USER=user PASS="xxx" ./misc/wiki-edit.sh -G # This creates a curl cookie in the CWD
# (normal usage afterwards)
#   ./misc/wiki_tasks_to_redmine.sh <(PAGE="ComitéSysAdmin/2019-09-11" ./misc/wiki-edit.sh -G)
#
# Utilisation un fichier text:
#   ./misc/wiki_tasks_to_redmine.sh "fichier"
#
# Pour ajouter un bout de texte supplémentaire, eg. de quelle réunione les demandes arrivent:
#   CAPTION="Action provenant de la réunion https://wiki.koumbit.net/X/YYYY-MM-DD" ./misc/wiki_tasks_to_redmine.sh "fichier"
#
PROJECT="kt-sysadmin"
FROM=
SMTP_SERVER="homere.koumbit.net"
SMTP_USER=
# This option can take values of "starttls" or "ssmtp". anything else disables
# TLS connexions for the SMTP session.
SMTP_TLS="ssmtp"
REDMINE_USER="redmine"
REDMINE_HOST="redmine.koumbit.net"
CAPTION=""
TICKET_BODY_CAPTION="Wiki to Redmine Migration $(date +%c)"

if [ -z "$1" ]; then
    echo "usage: $0 (<filename>|-)" >&2
    echo "  using - as a filename, the script will get the contents of the" >&2
    echo "  wiki page from stdin. This can let you grab the page with curl and pipe it" >&2
    echo "  in this script." >&2
    exit 1
fi

# Get configuration for the script
if [ -f ~/.config/koumbit/wiki_tasks_to_redmine.conf ]; then
    # shellcheck source=/dev/null
    . ~/.config/koumbit/wiki_tasks_to_redmine.conf
fi

if [ "$1" = "-" ] ; then
    FILE=/proc/self/fd/0
else
    FILE="$1"
fi

if [ -z "$FROM" ]; then
    printf "\\e[31merror\\e[0m: no FROM address found.\\n" >&2
    echo "Please create a configuration file in ~/.config/koumbit/wiki_tasks_to_redmine.conf with a line like the following:" >&2
    echo >&2
    echo "FROM=you@koumbit.org" >&2
    exit 1
fi

declare -A STATUSMAP=(
    ["en cours"]="In Progress"
    ["stagnant"]="In Progress"
    ["encours"]="In Progress"
    ["pas fait"]="New"
    ["à faire"]="New"
)
declare -A ASSIGNEEMAP=(
    ["Kienan"]="Kienan Stewart"
    ["sebas"]="Sébastien Grenier"
    ["sébas"]="Sébastien Grenier"
    ["tous"]=""
    ["nina"]="Nina Ripoll"
    ["gab"]="Gabriel Filion"
    ["lutin"]="Gabriel Filion"
    ["hubert"]="Hubert Pineault"
    ["john"]="John Béjot"
)

DRY_RUN=${DRY_RUN:-}
DEBUG=${DEBUG:-}

function debug {
    if [ -n "$DEBUG" ]; then
        printf "\\e[36mDEBUG\\e[0m: %s\\n" "$*" >&2
    fi
}

function map_status {
    local OLD=$1
    local i
    local NEW=''
    for i in "${!STATUSMAP[@]}" ; do
        #echo "key  : $i"
        #echo "old  : $OLD"
        if echo "$OLD" | grep -iq "$i" ; then
            NEW="${STATUSMAP[$i]}"
            break
        fi
    done
    if [ -z "$NEW" ] ; then
        echo "New"
    else
        echo "$NEW"
    fi
}

function map_assignee {
    local OLD=$1
    local i
    local NEW=''
    for i in "${!ASSIGNEEMAP[@]}" ; do
        if echo "$OLD" | grep -iq "$i" ; then
            NEW="${ASSIGNEEMAP[$i]}"
            break
        fi
    done
    if [ -z "$NEW" ] ; then
        echo ""
    else
        echo "$NEW"
    fi
}

if [ -n "$SMTP_USER" ]; then
    read -rsp "Please provide your smtp password: " SMTP_PASS
    # clear out the line since read doesn't do it
    echo
    if [ -z "$SMTP_PASS" ]; then
        echo "No password given. cannot move forward with requested smtp authentication." >&2
        exit 1
    fi
fi

while IFS= read -r LINE ; do
    if [[ ! "$LINE" =~ ^[[:space:]]+Action::[[:space:]]\[ ]]; then
        debug "skipping line: $LINE"
        continue
    else
        debug "action found on line: $LINE"
    fi

    ASSIGNEES=$(echo "$LINE" | grep -o ' Action:: \[.*\]' | grep -o '\[.*\]' | tr -d '[]')
    PRIMARY=$(echo "$ASSIGNEES" | cut -d ',' -f 1)
    OTHERS=$(echo "$ASSIGNEES" | cut -d ',' -s -f1 --complement)
    if [ "$OTHERS" == "$PRIMARY" ] ; then
        OTHERS=''
    fi
    debug "Assignees: $ASSIGNEES"
    debug "Primary Assignee: $PRIMARY"
    debug "Secondary Assignee(s): $OTHERS"
    STATUS=$(echo "$LINE" | grep -o -e "'''.*'''\$" | tr -d "'")
    if [ -z "$STATUS" ] ; then
        STATUS='New'
    fi
    debug "Status: $STATUS"
    # shellcheck disable=SC2001
    SUBJECT=$(echo "$LINE" | sed "s/^ Action:: \\[.*\\] \\(.*\\)$/\\1/")
    if [ "$STATUS" != "New" ] ; then
        debug "removing '''<status>''' from the subject"
        # Remove the '''<status>''' from the subject
        SUBJECT=$(echo "$SUBJECT" | rev | cut -d "'" -f 1,2,3,4,5,6 --complement | rev)
    fi
    debug "Subject: $SUBJECT"
    #map_status "$STATUS"
    MAPPEDSTATUS=$(map_status "$STATUS")
    MAPPEDASSIGNEE=$(map_assignee "$PRIMARY")
    debug "Mapped Status: ${MAPPEDSTATUS}"
    debug "Mapped Assignee: ${MAPPEDASSIGNEE}"
    # Touch up the secondary assignees:
    if [ -n "$OTHERS" ] ; then
        OTHERS="Secondary assignees: $OTHERS"
    fi
    MAIL_BODY=$(cat <<EOF
Assigned to: $MAPPEDASSIGNEE
Status: $MAPPEDSTATUS
Tracker: Task
$OTHERS

Original action: <pre>
$LINE
</pre>

$CAPTION
$TICKET_BODY_CAPTION
EOF
    )

    SMTP_AUTH=
    if [ -n "$SMTP_SERVER" ]; then
        SMTP_AUTH=("--server" "$SMTP_SERVER")
    fi
    if [ -n "$SMTP_USER" ]; then
        # XXX: the single quote transformation doesn't work for all passwords.
        # it can prevent login to work for some. need to identify why
        SMTP_AUTH+=("--auth-user" "${SMTP_USER}" "--auth-password" "'${SMTP_PASS//'/\\'}'")
    fi

    TLS=
    case $SMTP_TLS in
        starttls)
            TLS=("-tls")
            ;;
        ssmtp)
            TLS=("--protocol" "SSMTP")
            ;;
    esac

    TO="${REDMINE_USER}+${PROJECT}@${REDMINE_HOST}"
    # splitting array expansions in different arguments so that shellcheck is
    # happy.
    debug "Command: swaks" "${SMTP_AUTH[@]}" "${TLS[@]}" "--from \"${FROM}\" --header-Subject \"${SUBJECT}\" --to \"$TO\" --body \"${MAIL_BODY}\""
    if [ -z "${DRY_RUN}" ]; then
        echo "Creating ticket: ${SUBJECT}"
        swaks -4 "${SMTP_AUTH[@]}" "${TLS[@]}" --from "${FROM}" --header-Subject "${SUBJECT}" --to "$TO" --body "${MAIL_BODY}"
    else
        debug "Not running command since dry mode is activated"
    fi
done < "$FILE"
