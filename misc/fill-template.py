#!/usr/bin/python

# the bulk of this is a really fancy way of verifying arguments for use
# in templates. The heavy lifting is processed in hackish shell scripts

import getopt, sys, subprocess, os

debug=0

def usage(oblig, match):
  print "well, this script is filling tempaltes, it takes a template, and the"
  print "template might have arguments, that might be mandatory"

  print "this is the struct of mandatory args:"
  print oblig
  
  print "and this is the struct of longarg names (so the first struct make"
  print "sense):"
  print match

def main():
  runningdir = os.path.dirname(__file__)
  # the args that are standard for the program
  NORMAL_OPTS = [ 'u', 'h', 't', 'v' ]
  # a dictionnary of mandatory values for different types of
  # templates
  OPT_OBLIG = { 'DISK': [ 'M', 'm', 'S', 's', 'o' ], 'CORPS':
                [ 'p', 'n', 'o' ], 'PLATEFORME': [ 'n' ],
                'ESPRIT': [ 'c', 'n', 'd' ]}

  # the %X% needs to match the MATCH array below, so that they can be filled
  # with the supplied arguments
  NAMESPACES = { 'DISK': '%M%/%m%%S%/%s%', 'PLATEFORME':
                 'Plateformes/%n%', 'CORPS': 'Corps/%n%', 'ESPRIT':
                 'Esprits/%n%' }

  # the long argument version of the argument above
  MATCH = {
           'c': 'Corps=', 'd': 'Disks=', 'e': 'Esprit', 'h': 'help', 'm':
           'Model=', 'M': 'Manufacturer=', 'n': 'Nom=', 'o': 'OdooRef=', 'p':
           'Plateforme=', 'r': 'Racke=', 's': 'Serial=', 'S': 'size=',  't':
           'template=', 'T': 'TrouDeRack=', 'u': 'urlonly=', 'v': 'verbose'
          }
  raw_opt = []
  # merge all the allowable args for this scripts
  for a in OPT_OBLIG:
    raw_opt = raw_opt + OPT_OBLIG[a]
  #sets have no duplicates
  options = list(set(NORMAL_OPTS + raw_opt))
  getoptline = ''
  getoptlinelong = []
  # we need to generate long options and short options
  for o in options:
    getoptlinelong.append( MATCH[o].lower() )
    if MATCH[o].endswith('='):
      getoptline = getoptline + o + ':'
    else:
      getoptline = getoptline + o
  try:
      opts, args = getopt.getopt(sys.argv[1:], getoptline, getoptlinelong)
  except getopt.GetoptError as err:
      # print help information and exit:
      print str(err)  # will print something like "option -a not recognized"
      usage(OPT_OBLIG, MATCH)
      sys.exit(2)
  output = None
  verbose = False
  args = {}
  for a, b in opts:
    # strip - or --
    if a[0] == '-' and a[1] == '-':
      a = a[2:]
    elif a[0] == '-':
      a = a[1:]
    # if we get a long arg, we cut it to a short one
    if a in map(lambda x: x.lower(), MATCH.values()) or a + "=" in map(lambda x: x.lower(), MATCH.values()):
      for c in MATCH.keys():
        if MATCH[c] == a or MATCH[c] == a + "=":
          a = c
    # if we have a duplicate argument
    if a in args or (a in MATCH and MATCH[a] in args):
      print "You specified two times argument " + a + " or " + MATCH[a]
      sys.exit(3)
    else:
      # in all case we want a dict of the args we got:
      args.setdefault(a, b)
  if args.has_key('u') and len(args['u']) == 0:
    print "If you want to use url-only args, you need to specify which"
    sys.exit(3)
  if not args.has_key('u'):
    args['u'] = ''
  if not args.has_key('t'):
    print "you need to specify a template to work from"
    sys.exit(3)
  if args.has_key('h'):
    usage(OPT_OBLIG, MATCH)
    sys.exit(4)
  if args.has_key('v'):
    verbose = True
  if args['t'].upper() not in OPT_OBLIG.keys():
    print "template has to be one of these: " + str(OPT_OBLIG.keys())
    sys.exit(5)
  missing = False

  # we check if all the mandatory args are specified
  for i in OPT_OBLIG[args['t'].upper()]:
    if i not in args.keys():
      print "missing argument: " + MATCH[i][:-1]
      missing = True
  if missing:
    print "some arguments for the template you provided were not supplied"
    sys.exit(6)
  # we figure out if we have the arguments to make the page namespace
  name = NAMESPACES[args['t'].upper()]
  if name.count('%'):
    if name.count('%') % 2:
      print "The namespace for the specified type have a even number of % ; replacements"
      print "are made in %args%"
      sys.exit(7)
    end = 0
    tags = []
    # we replace all the %nom% by the value in the args, or fail
    while name.count('%'):
      begin = name.find('%', end)
      end = name.find('%', begin + 1)
      #print name[begin+1:end]
      if args.has_key(name[begin+1:end]):
        name = name[0:begin] + args[name[begin+1:end]] + name[end+1:]
      else:
        print "the template namespace need arg" + name[begin+1:end-1]
        sys.exit(8)
  # we properly processed the new page name to fit the documentation
  if subprocess.call([runningdir + "/wiki-edit.sh", "-p", name, "-e"]) == 0:
    # well page already exists, lets just use setfields
    fields = ""
    values = ""
    for i in args.keys():
      if i != 't' and i != 'u':
        # we need to skip args that are only in url
        if i not in args['u']:
          # for each field, lets change if its not set
          if not fields:
            fields = MATCH[i][:-1]
          else:
            fields = fields + "%" + MATCH[i][:-1]
          if not values:
            values = args[i]
          else:
            values = values + "%" + args[i]
    #TODO: well wiki-edit.sh does not have a return error, so we dont have a way of seeing if shit is fucked
    a = subprocess.call( [runningdir + "/set-field.sh", "-n", name, '-f',  fields, '-v', values ] )
    sys.exit(11)
  else:
    # we just populate the template
    # to test: print(subprocess.check_output(["./wiki-edit.sh", "-p", args['t'][5:1].upper()+args['t'][1:].lower()+'Template', "-e"]))
    if subprocess.call([runningdir + "/wiki-edit.sh", "-p", args['t'][0:1].upper()+args['t'][1:].lower()+'Template', "-e"]) == 0:
      # if the template exists we just send stuff out to fill-template.sh
      fields = ''
      values = ''
      sep = ''
      for i in args.keys():
        if i != 't':
          if fields != '':
            sep = '%'
          values = values + sep + args[i]
          fields = fields + sep + MATCH[i][:-1].upper()
      if subprocess.call( [runningdir + "/fill-template.sh", "-n", name, "-t", args['t'][0:1].upper()+args['t'][1:].lower(), '-f',  fields, '-v', values] ) != 0:
        print "fill-template.sh failed. called like this: " + str(["./fill-template.sh", "-n", name, "-t", args['t'][0:1].upper()+args['t'][1:].lower(), '-f',  fields, '-v', values])
        sys.exit(10)
    else:
      print "There is no page named" + args['t'][0:1].upper()+args['t'][1:].lower() + "Template"
      print "and the page name doesn't exist either"
      sys.exit(9)

if __name__ == "__main__":
    main()
