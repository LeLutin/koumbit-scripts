#!/bin/sh

# err, its waaaaaaaaaaaaaay simpler to code in sh

# to test:
#NAME="CabinetB227/49"
#TYPE=TrouDeRack
#FIELDS="SLOT RACK SWITCH SERIALPORT"
#VALUES="49 CabinetB227 sw1.koumbit.net 17"


usage() {
  cat <<EOF
This thing calls WikiEdit to fill a template ; its not validating 
inputs.

You need to specify those four things ; stuff is hackish for now

usage: $0 -t Type=> get data from \$TypeTemplate 
       $0 -p PageName => create PageName
       $0 -f 'field1 field2 field3' fields to be filled
       $0 -v 'value1 value2 value3' values associated to fields above
EOF
  exit
}

numArg=0

args=`getopt hn:t:f:v: $*`

set -- $args

for i
do
  case "$i"
  in
     -n)
          NAME=$2
          numArg=$((numArg+1))
          shift 2;;
     -t)
          TYPE=$2
          numArg=$((numArg+1))
          shift 2;;
     -f)
          FIELDS=$2
          echo $FIELDS
          numArg=$((numArg+1))
          shift 2;;
     -v)
          VALUES=$2
          numArg=$((numArg+1))
          shift 2;;
     -h)
          usage;
          exit;;
     --)
          shift; break;;
  esac
done

if [ "$numArg" != '4' ]
then
  echo $numArg
  usage
  exit
fi

BASEDIR=$(dirname "$0")

RUN=
COUNT=0
for I in $(echo $FIELDS | tr % ' ');
do
  COUNT=$(expr $COUNT + 1)
  #echo "Count: $COUNT"
  if [ "$RUN" = "" ] ;
  then
    RUN="sed -e 's/%%$I%%/$(echo $VALUES | cut -d%  -f$COUNT)/'";
  else
    RUN="$RUN | sed -e 's/%%$I%%/$(echo $VALUES | cut -d%  -f$COUNT)/'";
  fi
  #echo $RUN
done
TEMPLATE=$(${BASEDIR}/wiki-edit.sh -p ${TYPE}Template -G)

echo "$TEMPLATE" | eval $RUN | ${BASEDIR}/wiki-edit.sh -p $NAME -S
