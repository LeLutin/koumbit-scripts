"""Run functional tests for the moinmoin_client library.

This script will use all features of the moinmoin library to see if they all
work as expected. Verifications should be made manually afterwards on the
moinmoin instance.

Note that this script will make changes to one page, which name the user must
specify, and its renamed version (with "NewName" as a suffix).
"""
import logging

import requests

from moinmoin_client import Moin

if __name__ == "__main__":
    # We're debugging, so we need this output:
    logging.basicConfig(level=logging.DEBUG)

    logger = logging.getLogger("moinmoin_client")

    # Scary warning first. This is no unit test, it will change things in a a
    # live instance!
    print(
        """WARNING: Please note that this testing procedure will be disruptive.
It will perform a series of actions on the moinmoin instance, and specifically
on the page path that you provide. This means that the page used will be
modified and ultimately destroyed.""")

    # The instance URL and credentials can be set in the environment as:
    #   * MOIN_URL
    #   * MOIN_USER
    #   * MOIN_PASS
    #   * MOIN_TEST_PAGE
    #
    # If you want to avoid leaking your password to your shell history, you
    # can use the following:
    #   read -rsp "moinmoin pass: " MOIN_PASS; export MOIN_PASS
    #
    # Additionally, you can export an authentication session cookie in
    # MOIN_AUTH_COOKIE as one string containing the cookie name and the cookie
    # value separated by a color. This will avoid calling the login() method
    # which will make tests faster -- but will not test that the login()
    # method is functional.

    # Only importing down here: we usually don't need os for the library
    # itself
    import os
    moin_url = os.environ.get('MOIN_URL')
    moin_user = os.environ.get('MOIN_USER')
    moin_pass = os.environ.get('MOIN_PASS')
    moin_test_page = os.environ.get('MOIN_TEST_PAGE')
    moin_auth_cookie = os.environ.get('MOIN_AUTH_COOKIE')

    # Or we can also prompt for them each time:
    if moin_url is None:
        moin_url = input("Please provide a moinmoin instance base URL: ")
    if moin_user is None:
        moin_user = input("Username: ")
    if moin_pass is None:
        # Only importing down here: we usually don't need getpass for the
        # library itself
        import getpass
        moin_pass = getpass.getpass("Password: ")
    if moin_test_page is None:
        moin_test_page = input(
            "Path (without the base URL) of a page to run tests on: ")

    # Only importing down here: we usually don't need json for the
    # library itself
    import json

    # Let's test that the client works.
    if moin_auth_cookie is not None:
        creds = json.loads(moin_auth_cookie)
        logger.debug("Reusing cookie %s", creds)
        m = Moin(moin_url, cookies=creds)
    else:
        m = Moin(moin_url)

        cookies = m.login(moin_user, moin_pass)
        logger.info("""Login successful. Cookie grabbed.
If you want to reuse this cookie, you can use:
  export MOIN_AUTH_COOKIE='{}'""".format(json.dumps(cookies)))

    try:
        page_raw_content = m.page_raw(moin_test_page)
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == 404:
            print("The page {} doesn't exist yet.".format(moin_test_page))
            page_raw_content = ""
        else:
            raise

    print("Contents of the page before modification:")
    print("-------------8< START PAGE 8<---------------")
    print(page_raw_content)
    print("--------------8< END PAGE 8<----------------")

    page_raw_content += "This is a '''test'''.\n"

    # Only importing down here: we usually don't need datetime in the library
    # itself
    from datetime import datetime
    test_comment = "Test page edit on {}".format(str(datetime.now()))
    m.edit_page(moin_test_page, page_raw_content, test_comment)

    # Let's now re-read the page and see if it was changed
    try:
        page_new_content = m.page_raw(moin_test_page)
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == 404:
            print("The page {} doesn't exist yet.".format(moin_test_page))
            page_new_content = ""
        else:
            raise

    print("Contents of the page after modification:")
    print("-------------8< START PAGE 8<---------------")
    print(page_new_content)
    print("--------------8< END PAGE 8<----------------")

    # Diff everything from first to latest revisions
    print(m.diff(moin_test_page, 1, 0))

    # NOTE: this will start bugging out with the second iteration of tests and
    # onward. moinmoin has a weird concept of what exists where. The only way
    # to keep testing that rename works is to vary the test page name.
    m.rename_page(moin_test_page, moin_test_page + "NewName",
                  "testing a rename")

    m.delete_page(moin_test_page, "deleted: test run complete")
