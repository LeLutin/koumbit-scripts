#!/bin/sh

# Faut passer USER=asdf PASS=asdf PAGE wiki-edit.sh ; normalement juste une 
# fois, pcq ca va pogner le cookie
MODIFYBEGIN=##MACHINEEDITABLECONTENTBEGIN
MODIFYEND=##MACHINEEDITABLECONTENTEND
URL=https://wiki.koumbit.net
LOGIN_URL=$URL/action/login
COOKIE_JAR=.curl-local-cookie
RAW='?action=raw'

if [ -f ~/.wiki-editrc ]
then
  . ~/.wiki-editrc
fi

DEBUG=${DEBUG:-}

debug() {
    if [ -n "$DEBUG" ]; then
        printf "\\e[36mDEBUG\\e[0m: %s\\n" "$*" >&2
    fi
}

usage() {
  cat <<EOF
Faut passer USER=asdf PASS=asdf PAGE=page sh wiki-edit.sh [-p page] ; le user + pass, tu
peut le passer juste une fois, parce que le cookie est valide vraiment
longtemps...

You need to specify one of these five options ; stuff is hackish for now

usage: $0 -G => get data from the wiki (do NOT use, use g instead)
       $0 -S => set data in the wiki (do NOT use, use s instead)
       $0 -e => check if page exists in the wiki
       $0 -s => set modifyable data in the wiki
       $0 -p PageName => act on page PageName
       $0 -g => get modifyable data in the wiki
EOF
  exit
}

if [ $? != 0 ]
then
    usage
    exit 2
fi

args=`getopt eGSgshp: $*`

set -- $args

eflag=gflag=sflag=Gflag=Sflag=numFlag="0"

for i
do
  case "$i"
  in
      -e)
          eflag="1"
	  numFlag=$((numFlag+1))
          shift;;
      -s)
          sflag="1"
	  numFlag=$((numFlag+1))
          shift;;
      -g)
          gflag="1"
	  numFlag=$((numFlag+1))
          shift;;
      -S)
          Sflag="1"
	  numFlag=$((numFlag+1))
          shift;;
      -p)
          PAGE=$2
          shift 2;;
      -G)
          Gflag="1"
	  numFlag=$((numFlag+1))
          shift;;
      -h)
          usage;
          exit;;
      --)
          shift; break;;
  esac
done

if [ "$numFlag" != '1' ]
then
  usage
  exit
fi

IS403=$(curl -sD- -o/dev/null -b$COOKIE_JAR  "$URL/$PAGE$RAW" | grep -c 'HTTP/1.1 403')

if [ "$IS403" = '1' ]; then
  debug "Trying log in to get cookie"
  # XXX: the login won't work if the password contains certain characters (e.g. &)
  if curl -s "$LOGIN_URL" -c$COOKIE_JAR --data "action=login&name=$USER&password=$PASS&login=Login&login=Login" | grep "Invalid username or password"; then
    echo "\\e[31mLogin failed, impossible to get content\\e[0m" >&2
    exit 1
  fi
else
  debug "I'm already logged in LA LA LA LA LA"
fi

if [ "$Gflag" = "1" ]; then
  debug "Requesting page with acquired cookie"
fi

IS404=$(curl -sD- -o/dev/null -b$COOKIE_JAR  "$URL/$PAGE$RAW" | grep 'HTTP/1.1 404' | wc -l)

if [ $IS404 = "1" ] ;
then
  if [ "$Gflag" = "1" ] || [ "$gflag" = "1" ] ;
  then
    echo "Document does not exist on the wiki";
    exit 1;
  fi
  if [ $eflag = "1" ] ; # 404 and exists flag is set
  then
    exit 1;
  fi
fi

if [ $eflag = "1" ] ; # 404 and exists flag is set
then
  exit 0;
fi

if [ "$Gflag" = "1" ] ;
then
  curl -s -b$COOKIE_JAR  "$URL/$PAGE$RAW"
  exit
fi

if [ "$Sflag" = "1" ] ;
then
  # we download the file, we parse it to get the field to feed the submit
  SCRATCHFILE=$(mktemp)
  curl -s -o $SCRATCHFILE -b$COOKIE_JAR  "$URL/$PAGE?action=edit&editor=text"
  TICKET=$(cat $SCRATCHFILE | grep '<input type="hidden" name="ticket" value=' | sed  's/.*<input type="hidden" name="ticket" value="\([^"]*\)".*/\1/')
  REV=$(cat $SCRATCHFILE | grep '<input type="hidden" name="rev" value=' | sed  's/.*<input type="hidden" name="rev" value="\([^"]*\)".*/\1/')
  DRAFT_REV=$(cat $SCRATCHFILE | grep '<input type="hidden" name="draft_rev" value=' | sed  's/.*<input type="hidden" name="draft_rev" value="\([^"]*\)".*/\1/')
  DRAFT_TS=$(cat $SCRATCHFILE | grep '<input type="hidden" name="draft_ts" value=' | sed  's/.*<input type="hidden" name="draft_ts" value="\([^"]*\)".*/\1/')
  curl -s -o /dev/null -s -b$COOKIE_JAR "$URL/$PAGE#preview" --data "action=edit&rev=$REV&ticket=$TICKET&button_save=Save+Changes&draft_ts=$DRAFT_TS&draft_rev=$DRAFT_REV&editor=text&comment=&category=" --data-urlencode "savetext=$(cat)"
  rm $SCRATCHFILE
  exit
fi

# we're already exited if we had more generic function
if ! curl -s -b$COOKIE_JAR  "$URL/$PAGE$RAW" | grep -qe $MODIFYBEGIN -e $MODIFYEND ;
then
  echo "\\e[31mNo machine modifyable content on page :S.\\e[0m" >&2
  exit
fi

if [ "$gflag" = "1" ] ;
then
  #FIXME: this does not work if the first line if MODIFYBEGIN ; prolly also if last line is $MODIFYEND (TBV)
  curl -s -b$COOKIE_JAR  "$URL/$PAGE$RAW" | sed "1,/$MODIFYBEGIN/d;/$MODIFYEND/,\$d"
  exit
fi


if [ "$sflag" = "1" ] ;
then
  ORIGFILE=$(mktemp)
  FULLTHING=$(mktemp)
  curl -s -o $ORIGFILE -b$COOKIE_JAR  "$URL/$PAGE$RAW"
  cat $ORIGFILE | sed "/$MODIFYBEGIN/,\$d" > $FULLTHING
  echo $MODIFYBEGIN >> $FULLTHING
  cat >> $FULLTHING 
  echo $MODIFYEND >> $FULLTHING
  cat $ORIGFILE | sed "1,/$MODIFYEND/d" >> $FULLTHING
  cat $FULLTHING | PAGE=$PAGE sh $0 -S 
  rm $ORIGFILE $FULLTHING
  exit
fi


