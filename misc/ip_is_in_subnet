#!/usr/bin/python3
#
# This script checks that the first parameter (IP address) is within the CIDR
# prefix (subnet) described by the second parameter. IPv4 and IPv6 are
# supported.
#
# example use:
#
#    ./ip_is_in_subnet.py 199.58.80.192 199.58.80.0/24
#    ./ip_is_in_subnet.py 2602:FF21::F2 2602:FF31::/36
#

import sys
import os
import ipaddress


def usage():
    print("Usage: %s <ip_address> <cidr_prefix>" % sys.argv[0], file=sys.stderr)
    exit(-1)


if __name__ == '__main__':
    debug = (os.environ.get('DEBUG', '') != '')

    if len(sys.argv) < 3:
        usage()

    try:
        address = ipaddress.ip_address(sys.argv[1])
        net = ipaddress.ip_network(sys.argv[2])
    except ValueError as e:
        print(f"error: {e}\n")
        usage()

    if not address in net:
        if debug:
            print("not in subnet")
        exit(1)

    if debug:
        print("is in subnet")
