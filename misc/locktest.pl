#! /usr/bin/perl -w

use Fcntl qw(LOCK_SH LOCK_EX LOCK_UN);

$child = fork();
open(TESTLCK, ">testlock");

if ($child == 0) { # in child
	print "locking exclusively\n";
	flock(TESTLCK, LOCK_EX) || die "failed to lock exclusively: $!";
	print "holding exclusively lock for 3 seconds\n";
	sleep 3;
	flock(TESTLCK, LOCK_UN) || die "failed to unlock exclusively: $!";
	print "done locking exclusively\n";
} else { # in parent
	print "locking shared\n";
	flock(TESTLCK, LOCK_SH) || die "failed to lock shared: $!";
	print "holding shared lock for 3 seconds\n";
	sleep 3;
	flock(TESTLCK, LOCK_UN) || die "failed to unlock shared: $!";
	print "done locking shared, waiting for child to finish\n";
	wait;
}
