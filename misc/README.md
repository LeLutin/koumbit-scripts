# README

## Wiki tasks to Redmine

There's a bash script `wiki_tasks_to_redmine.sh` that should be called after sysadmin meetings to transform action points into redmine tickets.

### First time setup

We will be using shell1 as our base

```bash
# SSH onto shell1
ssh shell1

# Tickets will be created by you
export PRENOM="john"

# Set the prerequisites
mkdir -p ~/.config/koumbit/
echo "FROM=${PRENOM}@koumbit.org" > ~/.config/koumbit/wiki_tasks_to_redmine.conf

# Fetch the script
wget https://redmine.koumbit.net/projects/koumbit-scripts/repository/revisions/master/raw/misc/wiki_tasks_to_redmine.sh

# Remove TLS and point to localhost for SMTP configs
sed -e 's/SMTP_SERVER="homere.koumbit.net"/SMTP_SERVER="localhost"/' \
    -e 's/SMTP_TLS="ssmtp"/SMTP_TLS=""/' \
    -i wiki_tasks_to_redmine.sh
```

### Whenever you need to run the script

```bash
# SSH onto shell1
ssh shell1

# Specify meeting info
export DATE_REUNION="2020-11-03"
export NOM_REUNION="ComitéSysAdmin"

# Get the "text" version of your wiki page
#     * you can use the script `wiki-edit.sh` which requires entering your LDAP password in plaintext ...
#     * OR you can copy paste the notes from BBB / pad / wiki
#     * just make sure it's not the HTML but only MoinMoin syntax
# Enter paste mode in vim with `:set paste` and then paste the MoinMoin content
vim "${DATE_REUNION}.txt"

# Run the bash command with the right parameters
CAPTION="Action provenant de la réunion https://wiki.koumbit.net/${NOM_REUNION}/${DATE_REUNION}" ./wiki_tasks_to_redmine.sh "${DATE_REUNION}.txt"
```

