#!/usr/bin/python3
"""archive_subpages_by_year.py

This script will move all sub-pages of a given page, which look like dates and
which start with a given year number. The archival process is done simply by
make the pages into sub-pages of a pages called "archives". The "archives" page
is a sub-page of the page under which we'll find all date-named pages.

e.g.:

    SomePage/2019-01-10

becomes:

    SomePage/archives/2019-01-10

"""

import sys
from pathlib import Path
import re

from wiki import WikiCLI

# XXX manual list should be fetched automatically. but moinmoin really doesn't
# make it easy to do programatically :(
#   if we were to do this, we'd need a base page name and a year number as
#   arguments to the script instead of a file name
#
# For now, you can manually find the pages you want in moinmoin with a title
# regex like t:re:RapportDePaye/2020-.* and then drop them in a file, one name
# per line and give this script the name of the file as an argument
#
if __name__ == '__main__':

    if len(sys.argv) < 2:
        print("usage: archive_subpages_by_year.py <file_name>")
        exit(1)

    file_name = sys.argv[1]
    page_file = Path(file_name)
    if not page_file.is_file():
        print(f"errors: file {file_name} not found")
        exit(1)

    w = WikiCLI()

    with page_file.open() as f:
        pages = [x.strip() for x in f.readlines()]

        # Sanity check: page names should end with a full date
        rejected = [x for x in pages
                    if re.search("/[0-9]{4}-[0-9]{2}-[0-9]{2}$", x) is None]
        if len(rejected) > 0:
            print(f"error: Some pages did not end with a date of the form YYYY-MM-DD: {rejected}")  # noqa: E501
            exit(1)

    for orig_page in pages:
        elems = orig_page.split('/')
        # XXX this is supposing that we're always archiving something that's
        #   only two levels deep as SomePage/date
        elems.insert(1, 'archives')

        # TODO: sanity checks
        #   * grab base page, see if it exists
        #   * grab archives sub-page, see if it exists

        dest_page = '/'.join(elems)
        print(f"Renaming: {orig_page} -> {dest_page}")
        w.rename_page(orig_page, dest_page)
