#!/bin/sh

# ###template=DISK;manufacturer=WesternDigital;model=Gold;serial=v8hb23jr;size=6tb

WARNED_FILE=/home/gbeaulieu/odoo-warned
LOG_FILE=/home/gbeaulieu/odoo-log

if ! [ -f $WARNED_FILE ]
then
  touch $WARNED_FILE
fi

# for ticket labelled immediate and still new
echo "select body,id,record_name from mail_message where body like '<p>###template%' and model='purchase.order';" | su postgres - -c "psql -t kt-prod" | 
{
while IFS=' ' read LINE
do
  # we check if there are 
  #echo "$LINE"
  STRING=`echo "$LINE" | cut -d '|' -f 1 | sed -e 's/^[[:space:]]*<p>//' -e 's/<\/p>[[:space:]]*$//' -e 's/<br>/ /g'`
  ID=`echo "$LINE" | cut -d '|' -f 2 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
  PO_NUM=`echo "$LINE" | cut -d '|' -f 3 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
  if ! grep "@$ID@" $WARNED_FILE > /dev/null
  then
    if [ ! -z "$ID" ] 
    then
    echo "$ID $PO_NUM $STRING la fin"
    echo $(date) starting inserting values for PO $PO_NUM, comment ID: $ID >> $LOG_FILE
    for i in $STRING ;
      do
  	   echo $i 
	   for j in $(echo $i | tr ';' ' ') ;
	    do
		    echo "$j";
		    FIELD=$(echo $j | cut -d '=' -f 1)
		    case $FIELD in
			    \#\#\#template)
				    TEMPLATE=$(echo $j | cut -d '=' -f 2);
				    ;;
			    serial)
				    SERIAL=$(echo $j | cut -d '=' -f 2);
				    ;;
			    size)
				    SIZE=$(echo $j | cut -d '=' -f 2);
				    ;;
			    model)
				    MODEL=$(echo $j | cut -d '=' -f 2);
				    ;;
			    manufacturer)
				    MANUFACTURER=$(echo $j | cut -d '=' -f 2);
				    ;;
		    esac
	    done
	    if [ $TEMPLATE == "DISK" ] 
	    then
		    U=" -u SmM "
	    fi
	    /opt/bin/fill-template.py $U -t $TEMPLATE -s $SERIAL -S $SIZE -m $MODEL -M $MANUFACTURER -o $PO_NUM >> $LOG_FILE
  	# we make sure that bug 123 will show even if bug 1234 is in there using BOW-EOW sentinels
	  if ! grep "@$ID@" $WARNED_FILE > /dev/null
	  then
	    if [ -z "$ID" ]
	    then
	      echo "@$ID@" >> $WARNED_FILE
	    fi
	  fi
  
    done
  fi
  fi
done
}

