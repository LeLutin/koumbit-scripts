#!/bin/bash

# Some default values
MONITORING=NoMonitoring
PUPPET=NotInPuppet
REDMINEPROJECT=

while getopts ":s:c:m:p:r:" opt $@; do
  case $opt in
    s)
      if ! $(gnt-instance list | grep "^$OPTARG" > /dev/null 2>&1)
      then
        echo "Error: instance '$OPTARG' cannot be found on the cluster." >&2
        exit 1
      fi
      SERVER="$OPTARG"
      ;;
    c)
      CUSTOMER="$OPTARG"
      ;;
    m)
      if [[ "$OPTARG" != "NoMonitoring" ]] &&
         [[ "$OPTARG" != "MonitoredIrcOnly" ]] &&
         [[ "$OPTARG" != "Monitored" ]]
      then
        echo Error: Invalid value "'$OPTARG'" for monitoring. Should be one of NoMonitoring, MonitoredIrcOnly or Monitored. >&2
        exit 1
      fi
      MONITORING="$OPTARG"
      ;;
    p)
      if [[ "$OPTARG" != "NotInPuppet" ]] &&
         [[ "$OPTARG" != "PartiallyInPuppet" ]] &&
         [[ "$OPTARG" != "FullyInPuppet" ]]
      then
        echo Error: Invalid value "'$OPTARG'" for puppet. Should be one of NotInPuppet, PartiallyInPuppet or FullyInPuppet. >&2
        exit 1
      fi
      PUPPET="$OPTARG"
      ;;
    r)
      REDMINEPROJECT="$OPTARG"
      ;;
    :)
      usage;
      echo "Error: Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

usage() {
  echo "$0 -s <server_name> -c <customer_name> [-m (NoMonitoring|MonitoredIrcOnly|Monitored)] [-p (NotInPuppet|PartiallyInPuppet|FullyInPuppet)] [-r <redmine_project_url>]";
}

if [[ -z "$SERVER" ]] || [[ -z "$CUSTOMER" ]]
then
  usage;
  exit 1;
fi

HOST=$(gnt-cluster info | grep "^Cluster name:" | cut -d ':' -f 2 | sed -e 's/^ *//;s/ *$//')
if [[ -z "$HOST" ]]
then
  echo Error: could not identify cluster name >&2
  exit 1
fi

TEMPLATE=$(cat <<EOF

<<TableOfContents>>

= Technical details =

 Name:: %%NAME%%
 Function:: vserver dédié
 IP:: %%IP%%
 Location:: [[%%HOST%%]]
 Vserver type:: Ganeti
 Customer:: %%CUSTOMER%%
 Monitoring:: %%MONITORING%%
 Activation:: %%BOOTTIME%%
 Size:: %%DISKSIZE%%
 Ram:: %%RAMSIZE%%
 Cores:: %%CORES%%
 OS:: %%OS%%
 Configuration management:: %%PUPPET%%
 Redmine:: %%REDMINEPROJECT%%

= Notes =

## Inscrire le provenance de la demande création de la VM.

= Services =

<<FullSearch(t:Service @PAGE@)>>

= Backups =

''Indiquer si des backups de ce serveur sont faits, vers où, quel logiciel, à quelle fréquence et s'il y a des exceptions par rapport à la procédure habituelle à noter.''

= Interventions =

<<FullSearch(t:RapportsIntervention linkto:"@PAGE@")>>

= Subpages =

<<Navigation(children, 1)>>

----
CategoryServer

EOF
)

# Gather most information from Ganeti directly
STUFF="NAME IP DISKSIZE BOOTTIME CORES RAMSIZE OS"
MOARSTUFF=""
IFS=% read $STUFF <<<"$(gnt-instance list --units=g --separator=% -o 'name,ip,disk.size/0,ctime,be/vcpus,be/memory,os' | grep $SERVER)"
for I in $STUFF ;
do
  if [[ "$MOARSTUFF" = "" ]] ;
  then
    MOARSTUFF="sed -e 's/%%$I%%/${!I}/'";
  else
    MOARSTUFF="$MOARSTUFF | sed -e 's/%%$I%%/${!I}/'";
  fi
done

# Some more info that come from arguments to the script.
MOARSTUFF="$MOARSTUFF | sed -e 's/%%HOST%%/$HOST/;s/%%MONITORING%%/$MONITORING/;s/%%PUPPET%%/$PUPPET/;s/%%REDMINEPROJECT%%/$REDMINEPROJECT/'"
if [[ -n "$CUSTOMER" ]]
then
  MOARSTUFF="$MOARSTUFF | sed -e 's/%%CUSTOMER%%/$CUSTOMER/'"
fi

printf '%s\n' "$TEMPLATE" | eval $MOARSTUFF
