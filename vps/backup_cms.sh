#!/bin/bash
# Copyright 2018 Kienan Stewart <kienan@koumbit.org>
# Licensed under GPLv2+
#
# Arguments:
#   source_db_name
#   source_webroot
#   source_certificates
#   destination_host
#   destination_user
#   destination_db_name
#   destination_webroot
#   destination_certificates
#   post-actions:
#     wordpress_perms
#     drupal_perms (?)
#     apache_reload
#   libs: 0 or more extra files which are sourced for functions
#  Post-actions should have a function backup_cms_post_"$action"
#  Post-actions receive the arguments DESTINATION_USER DESTINATION_HOST DESTINATION_WEBROOT
#  A post action may have the form action:arg1;arg2;...;argN
#  In this case arg1 to argN will be passed to the function after the user, host and webroot.
#  Eg. remote_script:/path/to/script
# Example:
#  backup_cms --source-webroot=/var/www/example.com --destination-webroot=/var/www/example.com
#  --source-db-name production_db --destination-db-name backup_db_name --destination-user root
#  --destination-host backup.example.com --post-actions apache_reload,wp_
#
#
# Set defaults
VERBOSE=0
NOOP=0
SOURCE_DB_NAME=""
SOURCE_WEBROOT=""
SOURCE_CERTIFICATES=()
DESTINATION_DB_NAME=""
DESTINATION_HOST=""
DESTINATION_USER=""
DESTINATION_CERTIFICATES=()
POST_ACTIONS=()
LIBS=()

# Parse arguments
POSITIONAL=()
while [[ $# -gt 0 ]];
do
    key="$1"
    case "$key" in
        -v|--verbose)
            VERBOSE=1
            shift
            ;;
        -h|--help)
            HELP=1
            shift
            ;;
        -duser|--destination-user)
            DESTINATION_USER="$2"
            shift
            shift
            ;;
        -dhost|--destination-host)
            DESTINATION_HOST="$2"
            shift
            shift
            ;;
        -sdb|--source-db-name)
            SOURCE_DB_NAME="$2"
            shift
            shift
            ;;
        -ddb|--destination-db-name)
            DESTINATION_DB_NAME="$2"
            shift
            shift
            ;;
        -swr|--source-webroot)
            SOURCE_WEBROOT="$2"
            shift
            shift
            ;;
        -dwr|--destination-webroot)
            DESTINATION_WEBROOT="$2"
            shift
            shift
            ;;
        -p|--post-actions)
            IFS=',' read -r -a POST_ACTIONS <<< "$2"
            shift
            shift
            ;;
        -l|--libs)
            IFS=',' read -r -a LIBS <<< "$2"
            shift
            shift
            ;;
        -scrts|--source-certificates)
            IFS=',' read -r -a SOURCE_CERTIFICATES <<< "$2"
            shift
            shift
            ;;
        -dcrts|--destination-certificates)
            IFS=',' read -r -a DESTINATION_CERTIFICATES <<< "$2"
            shift
            shift
            ;;
        -n|--noop)
            NOOP=1
            shift
            ;;
        *)
            POSITIONAL+=("$1")
            shift
            ;;
    esac
done
set -- "${POSITIONAL[@]}"

function usage {
    if [ "$1" ] ; then
        echo "$1"
        echo ""
    fi
    echo "Usage: backup_cms [-h] [-v|--verbose] --source-db-name db_name --source-webroot /path/to/docroot"
    echo -e "\\t--source-certificates 'cert1path,cert2path' --destination-user USER --destination-host HOST"
    echo -e "\\t--destination-db-name DB_NAME --destionation-webroot /path/to/docroot --destination-certificates 'cert1path,cert2path'"
    echo -e "\\t[--post-actions action1,action2] [--libs file1,...]"
    echo ""
    echo "Argument details:"
    echo -e "\\t-h/--help\\tShow this message and exit"
    echo -e "\\t-v/--verbose\\tOutput more details"
    echo -e "\\t-sdb/--source-db-name\\tThe name of the database for the source. Assumes mysql can connect without arguments to dump it"
    echo -e "\\t-swr/--source-webroot\\tThe full path to the webroot to be copied"
    echo -e "\\t-scrts/--source-certificates\\tA comma-separated list of full paths to certificate files. They will be copied and mapped to the destinations in --destination-certificates"
    echo -e "\\t-duser/--destination-user\\tThe user to use on the destination connection. Must be able to connect via SSh with no password"
    echo -e "\\t-dhost/--destination-host\\tThe destination server (IP or hostname) to connect to"
    echo -e "\\t-ddb/--destination-dbname\\tThe name of the database to upload into. Assumes mysql on the destination host can connect without arguments"
    echo -e "\\t-dwr/--destination-webroot\\tThe path to which source-webroot will be copied to on the destination host"
    echo -e "\\t-dcrts/--destination-certificates\\tA comma-separated list of a full paths on the destination host to which source-certificates are mapped"
    echo -e "\\t-p/--post-actions\\tA comma-separated list of post-actions to run. Post actions are functions defined here or in loaded libs for the form backup_cms_post_<action_name>"
    echo -e "\\t-l/--libs\\tA comma-separated list of full paths to files to source for running post-actions"
    echo -e "\\t-n/--noop\\tDo not perform of the operations (post actions will not be called either"
    echo ""
}

# Check usage

if [ "$HELP" ] ; then
    usage
    exit
fi

#echo "Postitional: $POSITIONAL"
#echo "Positional arguments: ${#POSITIONAL[@]}"
if [[ ${#POSITIONAL[@]} -ne 0 ]] ; then
    usage "Unknown arguments: ${POSITIONAL[*]}"
    exit 1
fi

for i in SOURCE_DB_NAME SOURCE_WEBROOT DESTINATION_HOST DESTINATION_USER DESTINATION_DB_NAME ; do
    if [ -z ${!i} ] ; then
        usage "$i is not defined and is required"
        exit 2
    fi
done

if [[ ${#SOURCE_CERTIFICATES[@]} -ne ${#DESTINATION_CERTIFICATES[@]} ]] ; then
    echo "Source certificates and destination certificates do not have the same amount of files listed."
    echo "The mapping of files for certificates should be 1 to 1"
    exit 3
fi

if [ ! -d "$SOURCE_WEBROOT" ] ; then
    echo "Source webroot doesn't exist"
    exit 4
fi

if [ "$SOURCE_CERTIFICATES" ] ; then
    for ((i=0;i<${#SOURCE_CERTIFICATES[@]};i++)) ; do
        src="${SOURCE_CERTIFICATES[$i]}"
        if [ ! -f "$src" ] ; then
            echo "Source certificate $src doesn't exist"
            exit 5
        fi
    done
fi

function backup_cms_apache_reload {
    ssh "$1"@"$2" service apache2 graceful
}

function backup_cms_remote_script {
    ssh "${1}@${2}" ${4} "${@:5}"
}

for i in ${LIBS[*]} ; do
    if [ ! -f "$i" ] ; then
        echo "Lib file '$i' doesn't exist"
        exit 6
    else
        if [ ! -z "$VERBOSE" ] ; then
            echo "Sourcing file '$i'"
        fi
        if [[ "$NOOP" -eq 0 ]] ; then
            source "$i"
        fi
    fi
done

for i in ${POST_ACTIONS[*]} ; do
    i=$(echo "$i" | cut -d ':' -f 1)
    if [[ $(type -t "backup_cms_$i") != "function" ]] ; then
        echo "Post action '$i' couldn't find a function matching 'backup_cms_$i'"
        if [[ "$NOOP" -eq 0 ]] ; then
            exit 7
        fi
    fi
done

function finish {
    rm -f "${DB_DUMP}.gz"
    if [[ "$NOOP" -eq 0 ]] ; then
        ssh "${DESTINATION_USER}@${DESTINATION_HOST}" rm -f "$DEST_DB_DUMP"
    fi
}
trap finish EXIT

DB_DUMP=$(mktemp)
if [[ "$VERBOSE" -eq 1 ]] ; then
    echo "Created tempfile for dbdump: $DB_DUMP"
fi

if [[ "$NOOP" -eq 0 ]] ; then
    mysqldump "$SOURCE_DB_NAME" > "$DB_DUMP"
    gzip "$DB_DUMP"
    DEST_DB_DUMP=$(ssh "$DESTINATION_USER"@"$DESTINATION_HOST" mktemp)
    if [[ "$VERBOSE" -eq 1 ]] ; then
        echo "Create tempfile for destination db_dump: ${DESTINATION_HOST}:${DEST_DB_DUMP}"
    fi
else
    echo "mysqldump \"$SOURCE_DB_NAME\" > \"$DB_DUMP\""
    echo "gzip \"$DB_DUMP\""
    echo "ssh \"${DESTINATION_USER}@${DESTINATION_HOST}\" mktemp"
    DEST_DB_DUMP="/tmp/noop-faketempfile"
fi
if [[ "$VERBOSE" -eq 1 ]] ; then
    set -x
fi

EXTRA_RSYNC_ARGS=""
if [[ "$VERBOSE" -eq 1 ]] ; then
    EXTRA_RSYNC_ARGS="-v"
fi

if [[ "$NOOP" -eq 0 ]] ; then
    scp "${DB_DUMP}.gz" "$DESTINATION_USER"@"$DESTINATION_HOST":"${DEST_DB_DUMP}.gz"
    rsync "$EXTRA_RSYNC_ARGS" -azE --delete "$SOURCE_WEBROOT"/ "$DESTINATION_USER"@"$DESTINATION_HOST":"$DESTINATION_WEBROOT"/
    ssh "${DESTINATION_USER}@${DESTINATION_HOST}" gunzip -f "${DEST_DB_DUMP}.gz"
    ssh "${DESTINATION_USER}@${DESTINATION_HOST}" "mysql \"$DESTINATION_DB_NAME\" < \"$DEST_DB_DUMP\""
else
    echo "scp \"${DB_DUMP}.gz\" \"${DESTINATION_USER}@${DESTINATION_HOST}:${DEST_DB_DUMP}.gz\""
    echo "rsync \"$EXTRA_RSYNC_ARGS\" -azE --delete \"${SOURCE_WEBROOT}/${DESTINATION_USER}@${DESTINATION_HOST}:${DESTINATION_WEBROOT}/\""
    echo "ssh \"${DESTINATION_USER}@${DESTINATION_HOST}\" gunzip -f \"${DEST_DB_DUMP}.gz\""
    echo "ssh \"${DESTINATION_USER}@${DESTINATION_HOST}\" \"mysql \\\"$DESTINATION_DB_NAME\\\" < \\\"$DEST_DB_DUMP\\\"\""
fi
if [ "$SOURCE_CERTIFICATES" ] ; then
    for ((i=0;i<${#SOURCE_CERTIFICATES[@]};i++)) ; do
        src="${SOURCE_CERTIFICATES[$i]}"
        dest="${DESTINATION_CERTIFICATES[$i]}"
        if [[ "$NOOP" -eq 0 ]] ; then
            scp "$src" "$DESTINATION_USER"@"$DESTINATION_HOST":"$dest"
        else
            echo "scp \"$src\" \"${DESTINATION_USER}@${DESTINATION_HOST}:${dest}\""
        fi
    done
fi
if [ "$POST_ACTIONS" ] ; then
    for i in ${POST_ACTIONS[*]} ; do
        case "$i" in
            *:*)
                action=$(echo "$i" | cut -d ':' -f 1)
                args=$(echo "$i" | cut -d ':' -f 2)
                IFS=';' read -r -a args <<< "$args"
                ;;
            *)
                action="$i"
                args=()
        esac
        if [[ "$NOOP" -eq 0 ]] ; then
            "backup_cms_$action" "$DESTINATION_USER" "$DESTINATION_HOST" "$DESTINATION_WEBROOT" ${args[*]}
        else
            echo "\"backup_cms_${action}\" \"$DESTINATION_USER\" \"$DESTINATION_HOST\" \"$DESTINATION_WEBROOT\" ${args[*]}"
        fi
    done
fi
exit 0
