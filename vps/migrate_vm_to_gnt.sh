#!/bin/bash
#
# This code is licensed under GPLv3+
#
# This script is the main structure for transferring a VM from another host (or another cluster) to the current ganeti cluster.
#
# NOTE: You should be running this script on the destination master node. some
# form of verification will be made, but the script can't know if a "master
# node" is in the destination cluster.
#

usage() {
    echo "Usage: $0"
    echo -e "\t --src-host SRC_HOST --src-lv SRC_LV_PATH --src-vm SRC_VM_NAME [--src-has-parittions]"
    echo -e "\t [--src-uses-ganeti] [--src-cluster-master SRC_CLUSTER_MASTER]"
    echo -e "\t --dst-host DST_NODE --dst-vm DST_VM_NAME [--post-migration-nics POST_MIGRATION_NICS]"
    echo -e "\t [--final] [-v | --verbose] [-y | --yes]"
    echo ""
    echo "Without -y/--yes the program will perform a dry run of the steps"
    echo "If verbose is set, the commands to be executed will be output"
    echo ""
    echo "When --final is set, the source VM will be shutdown and once the data transfer is finished"
    echo "GRUB will be installed and any last minute data changes will be made."
    echo ""
    echo "Post migration NICs will be added, if set, from index 0. The value of the argument should"
    echo "have the form: ip=192.168.13.12,network=net-name.example.com"
    echo "Multiple NICs may be added by separating the definitions with ';'"
    exit 2
}

## start variables
# Eg. hosting.example.com
SRC_HOST=""
# Eg. vm/foo or data/foo
SRC_LV_PATH=""
# Eg. hosting-new.example.com
DST_NODE=""
# Eg. vm.example.com
SRC_VM_NAME=""
DST_VM_NAME=""
# 0: the device is used directly for the FS, anything else: the device should be activated
# and the first partition mounted on the tempd dir
SRC_HAS_PARTITIONS=0
# 0: no, anything else: yes
# this changes the shutdown behaviour
SRC_USES_GANETI=0
# The host name of the ganeti cluster master for running commands
# Required if SRC_USES_GANETI is non-zero
SRC_CLUSTER_MASTER=""
# 0: do it, anything else: dry-run
DRY_RUN=1
# 0: quiet, anything else: verbose
VERBOSE=1
# 0: do not do final steps (default), anything else: do final steps
# final steps include shutting down the source VM, installing grub, last minute
# data massaging etc.
FINAL=0
# For adding NICs post-migration
# ip=XXX,network=XXX[;...]
POST_MIGRATION_NICS=""
## end variables
GNT_INSTALL_GRUB=/root/gnt_install-grub

PARSED_ARGUMENTS=$(getopt -n "$0" -o vy -l src-has-partitions,src-host:,src-lv:,src-vm:,dst-host:,dst-vm:,verbose,yes,src-uses-ganeti,src-cluster-master:,final,post-migration-nics: -- "$@")
# shellcheck disable=SC2181
if [ "$?" != "0" ] ; then
    usage
fi
# Replace shell arguments with parsed copy
eval set -- "$PARSED_ARGUMENTS"
while :
do
    case "$1" in
        -v | --verbose) VERBOSE=1 ; shift ;;
        -y | --yes) DRY_RUN=0 ; shift ;;
	--final) FINAL=1 ; shift ;;
        --src-host) SRC_HOST="$2" ; shift 2 ;;
        --src-lv) SRC_LV_PATH="$2" ; shift 2 ;;
        --src-has-partitions) SRC_HAS_PARTITIONS=1 ; shift ;;
        --src-vm) SRC_VM_NAME="$2" ; shift 2 ;;
        --src-uses-ganeti) SRC_USES_GANETI=1 ; shift ;;
        --src-cluster-master) SRC_CLUSTER_MASTER="$2" ; shift 2 ;;
        --dst-host) DST_NODE="$2" ; shift 2 ;;
        --dst-vm) DST_VM_NAME="$2" ; shift 2 ;;
	--post-migration-nics) POST_MIGRATION_NICS="$2" ; shift 2 ;;
        # End of args
        --) shift; break;;
        *) echo "Unexpected option '$1'" ; usage ;;
    esac
done

# Make sure we go the variables we need
REQUIRED_VARS="SRC_VM_NAME SRC_HOST SRC_LV_PATH DST_NODE DST_VM_NAME"
for i in $REQUIRED_VARS ; do
    if [ -z "${!i}" ] ; then
        echo "Required variable '$i' is not set"
        usage
    fi
done
if [[ "$SRC_USES_GANETI" -eq "1" ]] ; then
    if [ -z "$SRC_CLUSTER_MASTER" ] ; then
        echo "--src-cluster-master must be set when --src-uses-ganeti is given"
        usage
    fi
fi


# Takes in a disk path and outputs the path formatted for /dev/mapper/
function dev_mapper_name() {
  # shellcheck disable=SC2001
  echo "$1" | sed 's/-/--/g'
}

# Outputs a colored message (all parameters)
function progress() {
  >&2 echo -e "\\e[36m$*\\e[0m"
}

# Outputs a colored message (all parameters), but only if verbose mode is active
function verbose() {
  if [[ "$VERBOSE" -ne 0 ]]; then
    >&2 echo -e "\\e[97m$*\\e[0m"
  fi
}

# Run a command via ssh on a host. The hostname should be given as first param
# and the command as all params following the host.
function remote_command() {
  COMMAND_HOST=$1
  shift
  COMMAND=$*
  verbose "$COMMAND_HOST: $COMMAND"
  if [[ "$DRY_RUN" -eq 0 ]]; then
    ssh "$COMMAND_HOST" -- "$COMMAND"
  fi
}

# Two functions that are just syntactic sugar to make the code more easily readable
# All parameters make up the command that's run on a certain host.
function src() {
  remote_command "$SRC_HOST" "$*"
}
function dst() {
  remote_command "$DST_NODE" "$*"
}

# Setup mounts and create snapshot on source host. No parameter needed.
#
# NOTE: currently this is only implemented for an old xen-only source. we will
# probably need to be able to replace the src_host commands to make the script
# work for transferring from a xen ganeti cluster node.
function prepare_src_and_dst_dirs() {
  progress "Exposing destination root partition and mounting it in tmp dir"
  dst "kpartx -av $DST_ROOT_DISK"
  # shellcheck disable=SC2001
  ROOT_PART_PATH=/dev/mapper/$(echo "$DST_ROOT_DISK" | sed 's#^/dev/##;s#/.*##')-$(dev_mapper_name "$(echo "$DST_ROOT_DISK" | sed 's#^/.*/##')")p1
  dst "mount ${ROOT_PART_PATH} $DST_TMP_DIR"

  progress "Creating source snapshot and mounting it in tmp dir"
  src "lvcreate --snapshot --name $SNAPSHOT_NAME --size 30g $SRC_LV_PATH"
  SRC_DEVICE="/dev/mapper/$SRC_VG_NAME-$(dev_mapper_name "$SNAPSHOT_NAME")"
  if [[ ! "$SRC_HAS_PARTITIONS" -eq 0 ]] ; then
      src "kpartx -av /dev/mapper/$SRC_VG_NAME-$(dev_mapper_name "$SNAPSHOT_NAME")"
      SRC_DEVICE="${SRC_DEVICE}p1"
  fi
  src "mount $SRC_DEVICE $SRC_TMP_DIR"
  if ! src "findmnt $SRC_TMP_DIR" ; then
      progress "Failed to find mount at temp directory '$SRC_TMP_DIR'"
      cleanup
      exit 1
  fi
}

# Run a data transfer. No parameter needed.
#
# NOTE: This function supposes that prepare_src_and_dst_dirs was called
# successfully once beforehand.
function transfer_files() {
    # We're using rsync here since it's a reliable way of copying the entirety of
    # a host from one side to another. Also, it's a transfer command that can be
    # used to prime the destination with an initial transfer, and then also run
    # the final transfer which will end up taking way less time than if we were
    # to dd everything across the wire with the VM offline.
    # optional: add --bwlimit=75m
    if [ -z "$SRC_TMP_DIR" ] ; then
        progress "SRC_TMP_DIR is empty; aborting to avoid copying $SRC_HOST's root directory"
        return 1
    fi
    dst "rsync -azHAXx --sparse --whole-file --numeric-ids --info=progress2 --delete --exclude=/proc/* --exclude=/sys/* --exclude=/dev/* --exclude=/run/* $SRC_HOST:$SRC_TMP_DIR/ $DST_TMP_DIR/"
}

# Clean up after ourselves. No parameter needed.
function cleanup() {
  progress "Cleaning up\n"

  # we should probably accumulate cleanup commands in other "step functions"
  # and run the accumulated commands here instead of hardcoding things here.
  # This would mean that interrupting the script at any time would make sure to
  # remove what each steps already setup.

  progress "Unmounting and removing snapshot on source host"
  src "umount $SRC_TMP_DIR"
  src "rmdir $SRC_TMP_DIR"
  if [[ ! "SRC_HAS_PARTITIONS" -eq 0 ]] ; then
      src "kpartx -d /dev/mapper/$SRC_VG_NAME-$(dev_mapper_name "$SNAPSHOT_NAME")"
  fi
  src "lvremove -f $SRC_VG_NAME/$SNAPSHOT_NAME"

  progress "Unmounting and closing device on destination host"
  dst "umount $DST_TMP_DIR"
  dst "rmdir $DST_TMP_DIR"
  dst "kpartx -d $DST_ROOT_DISK"

  # this errors out on an offline instance, so we might just want to get rid of this command.
  #gnt-instance deactivate-disks "$DST_VM_NAME"
}

function shutdown_vm() {
    VM_NAME="$1"
    if [[ "$SRC_USES_GANETI" -eq "0" ]] ; then
        # shutdown the VM
        src "xen shutdown -w ""$VM_NAME"
        src "rm /etc/xen/auto/$VM_NAME.cfg"
    else
        remote_command "$SRC_CLUSTER_MASTER" "gnt-instance" "stop" "$VM_NAME"
        progress "Waiting for shutdown of source VM to finish\n"
        UP=1
        while [[ "$UP" -eq "1" ]] ; do
            sleep 5
            verbose "Checking if source VM is still up..."
            RESULT=$(remote_command "$SRC_CLUSTER_MASTER" gnt-instance list -o status "$VM_NAME" | tail -n 1)
            if [[ "$RESULT" =~ "running" ]] || [[ "$RESULT" =~ "_up" ]] ; then
                UP=1
                verbose " status is '$RESULT'"
            else
                UP=0
                break
            fi
        done
	remote_command "$SRC_CLUSTER_MASTER" "gnt-instance" "modify" "--offline" "$VM_NAME"
    fi
}


# grab VG name from LV path
# shellcheck disable=SC2001
SRC_VG_NAME=$(echo "$SRC_LV_PATH" | sed 's#/.*$##')
# create snapshot name from given lv path
# shellcheck disable=SC2001
SNAPSHOT_NAME=$(echo "$SRC_LV_PATH" | sed 's#^.*/##')-snapshot-$(date +%s)

## LET'S BEGIN

if [[ "$DRY_RUN" -eq "1" ]] ; then
  progress "##########################"
  progress "RUNNING IN DRY-RUN MODE"
  progress "NO COMMAND WILL BE APPLIED"
  progress "##########################"
fi

# PRE-FLIGHT CHECK
# gnt_install-grub (GNT_INSTALL_GRUB) needs to exist on destination server
# for this to work. Bail out if it doesn't exist.
progress "Verifying that '$GNT_INSTALL_GRUB' is available on '$DST_NODE'"
if ! dst "test -f $GNT_INSTALL_GRUB" ; then
    progress "File $GNT_INSTALL_GRUB does not exist, halting."
    exit 1
fi

# XXX that's a bit fucky... it should be a in prep() function but we're setting
# up global variables here that are used everywhere else. -- but if we
# accumulate the umount and rmdir commands for the tmp dirs from within
# prepare_src_and_dst_dirs, we don't need a global var anymore for those
# directories. so the solution here depends on cleanup accumulation of
# commands.
progress "Creating tmp dirs on both hosts"
# BUG: when running in dry-run mode the mktemp commands are not run so both
# variables are empty, leading to lots of confusion and broken commands down
# below.
# Prep tmp directories on both hosts
SRC_TMP_DIR=$(src 'mktemp -d')
DST_TMP_DIR=$(dst 'mktemp -d')
verbose "Source temp directory: $SRC_TMP_DIR"
verbose "Destination temp directory: $DST_TMP_DIR"

# TODO We might want to create the destination VM here? ..

# XXX either parse the following into the variables DST_ROOT_DISK (disk0) and
# DST_SWAP_DISK (disk1), or get the info from the RAPI (more reliable than
# parsing output from an interactive command, but for that to work we'll need
# to setup authentication and have a way to communicate auth info to this
# script)
#
# gnt-instance activate-disks vm.example.com
# hosting.example.com:disk/0:/dev/data/9f1f8d23-a51a-4e21-833e-9ecc51299016.disk0
# hosting.example.com:disk/1:/dev/data/241719d8-15df-4095-b105-8d383d7f5b92.disk1
#
# @BUG: This doesn't respect DRY_RUN
while read -r line ;
do DISKID=$(echo "$line" | cut -d ':' -f 2 | cut -d '/' -f 2)
   DISKPATH=$(echo "$line" | cut -d ':' -f 3)
   if [[ "$DISKID" -eq "0" ]] ; then
       DST_ROOT_DISK="$DISKPATH"
   elif [[ "$DISKID" -eq "1" ]] ; then
       DST_SWAP_DISK="$DISKPATH"
   else
       verbose "Disk ID out of range 0-1 from '${line}'"
   fi
done < <(gnt-instance activate-disks "$DST_VM_NAME")


if [[ ! "$FINAL" -eq "0" ]] ; then
  # Shutdown source VM
  progress "Final transfer requested\n"
  progress "Shutting down source VM\n"
  shutdown_vm "$SRC_VM_NAME"
fi

progress "setting up snapshot and mounts on both hosts\n"
prepare_src_and_dst_dirs
if [[ "$?" != "0" ]] ; then
    progress "Setup of src and dst dirs returned non-zero exit code. Stopping."
    cleanup
    exit 1
fi

# Run initial transfer while the VM is still live. This will make it so that
# the actual downtime is shorter.
progress "Data transfer\n"
progress "TEMPORARY NOTE (remove this if ever we find a workaround for the gnt-install_grub script): while this is running, make sure to verify in the VM that package upgrades are all done and that no upgrade will need to be done during the grub_install step"
transfer_files
if [[ "$?" != "0" ]] ; then
    progress "Rsync returned non-zero exit code. Stopping."
    cleanup
    exit 1
fi

if [[ ! "$FINAL" -eq "0" ]] ; then
  progress "Making some corrections to destination files in order to accomodate new device names\n"
  # make_destination_adjustments # (eg. remove hvc0, ... )
fi

# We can unmount everything now that data transfers and modifications are finished.
# The install_grub will manage it's own remounting
cleanup

function install_grub() {
  # Args: VM name, data block device, swap block device
  dst "$GNT_INSTALL_GRUB" -i "$1"  -b "$2" -s "$3"
}

if [[ ! "$FINAL" -eq "0" ]] ; then
  progress "Installing grub on destination VM\n"
  install_grub "$DST_VM_NAME" "$DST_ROOT_DISK" "$DST_SWAP_DISK"

  # Add any NICs necessary
  # gnt-instance modify --net N:add,ip=xxx,network=xxx $DST_VM_NAME
  NICID=0
  progress "Adding any post-migration NICs"
  while read -r line ; do
    if [ -z "$line" ] ; then
      continue
    fi
    verbose "gnt-instance modify --net $NICID:add,$line $DST_VM_NAME"
    if [[ "$DRY_RUN" -eq "0" ]] ; then
      gnt-instance modify --net "$NICID:add,$line" "$DST_VM_NAME"
    fi
    NICID=$((NICID + 1))
  done < <(echo "$POST_MIGRATION_NICS" | tr ';' $'\n')

  progress "Starting migrated instance\n"
  verbose "gnt-instance startup $DST_VM_NAME"
  if [[ "$DRY_RUN" -eq "0" ]] ; then
    gnt-instance startup "$DST_VM_NAME"
  fi
fi
