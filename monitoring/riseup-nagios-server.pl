#!/usr/bin/perl -w

# ##############################################################################
# a simple IRC bot which dispatches messages received via local domain sockets
# ##############################################################################

use strict;
use File::Basename;

BEGIN {
	unshift @INC, dirname($0);
}

my $VERSION = '0.2';
my $running = 1;

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# >> CONFIGURATION >>

my $cfg = {
	socket   => '/var/run/nagios/nsa.socket',
	server   => 'irc.freenode.net',
	port     => '6667',
	nickname => 'knag',
	password => '',
	channel  => '#koumbit',
	pidfile  => '/var/run/nagios/nsa.pid', # set to undef to disable
	realname => "Koumbit Nagios nagger (NSA $VERSION)",
};

# << CONFIGURATION <<
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

use POSIX qw(setsid);
use IO::Socket;
use Net::IRC;

sub new {
	my $self = {
		socket => undef,
		irc => undef,
		conn => undef
	};

	return bless($self, __PACKAGE__);
}

sub daemonize {
	my $self = shift;
	my $pid;

	chdir '/' or die "Can't chdir to /: $!";

	open STDIN, '/dev/null' or die "Can't read /dev/null: $!";
	open STDOUT, '>/dev/null' or die "Can't write to /dev/null: $!";

	defined ($pid = fork) or die "Can't fork: $!";

	if ($pid && $cfg->{pidfile}) { # write pid of child
		open PID, ">$cfg->{pidfile}" or die "Can't open pid file: $!";
		print PID $pid;
		close PID;
	}
	exit if $pid;
	setsid or die "Can't start a new session: $!";

	#open STDERR, '>&STDOUT' or die "Can't dup stdout: $!";
}

sub run {
	my $self = shift;

	$self->{irc}->do_one_loop();
}

sub shutdown {
	my $sig = shift;

	print STDERR "Received SIG$sig, shutting down...\n";
	$running = 0;
}

sub socket_has_data {
    my $self = shift;
    
    $self->{socket}->recv(my $data, 1024);
    $self->{conn}->privmsg($cfg->{channel}, $data);
}

sub irc_on_connect {
	my $self = shift;

	print STDERR "Joining channel '$cfg->{channel}'...\n";
	$self->join($cfg->{channel});
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

my $bot = &new;

if (-e $cfg->{socket}) {
	die "Socket '$cfg->{socket}' exists!\n";
}

$bot->{socket} = IO::Socket::UNIX->new (
	Local  => $cfg->{socket},
	Type   => SOCK_DGRAM,
	Listen => 5
) || die "Can't create socket '$cfg->{socket}'!\n";

$SIG{INT} = $SIG{TERM} = \&shutdown;

$bot->daemonize();
$bot->{irc} = new Net::IRC;

$bot->{conn} = $bot->{irc}->newconn (
	Server   => $cfg->{server},
	Port     => $cfg->{port},
	Nick     => $cfg->{nickname},
	Username => $cfg->{nickname},
	Password => $cfg->{password},
	Ircname  => $cfg->{realname},
) || die "Can't connect to server '$cfg->{server}'!\n";

$bot->{conn}->add_global_handler(376, \&irc_on_connect);
$bot->{conn}->add_global_handler('nomotd', \&irc_on_connect);
$bot->{irc}->addfh($bot->{socket}, \&socket_has_data, 'r', $bot);

while ($running) {
	$bot->run();
}

close($bot->{socket});
unlink($cfg->{socket});

exit(0);

1;

__END__
