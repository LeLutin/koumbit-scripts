### Author: Antoine Beaupre <anarcat@koumbit.org>

import re

class dstat_plugin(dstat):
    """
    Apache statistics plugin
    """

    def __init__(self):
        self.name = 'apache'
        self.nick = ('hits','busy','idle')
        self.vars = ('hits','busy','idle')
        self.type = 'd'
        self.width = 4
        self.scale = 100
        self.hitcount = 0
        #self.bwcount = 0

    def check(self):
	try:
            global httplib
            import httplib
        except:
            raise Exception, 'Plugin needs the httplib module'
        self.conn = httplib.HTTPConnection('localhost',8666)
        self.conn.connect()

    def extract(self):
        try:
            self.conn.request('GET', '/server-status')
            status = self.conn.getresponse().read()
            # ceres:/usr/share/dstat# curl -s http://ceres.koumbit.net:8666/server-status | grep 'Total accesses'
            # <dt>Total accesses: 5072 - Total Traffic: 35.9 MB</dt>
            m = re.search('<dt>Total accesses: (\d+) - Total Traffic: ([\d.]+) \w+', status)
            hitcount = int(m.group(1))
            #bwcount = float(m.group(2))
            if self.hitcount == 0:
               self.hitcount = hitcount
            #if self.bwcount == 0:
            #   self.bwcount = bwcount
            self.val['hits'] = hitcount - self.hitcount
            #self.val['bw'] = bwcount - self.bwcount
	    self.hitcount = hitcount
	    #self.bwcount = bwcount
            # ceres:/usr/local/share/dstat# curl -s  http://localhost:8667/server-status | grep processed
            # <dt>8 requests currently being processed, 25 idle workers</dt>
            m = re.search('<dt>(\d+) requests currently being processed, (\d+) idle workers</dt>', status)
            self.val['busy'] = int(m.group(1))
            self.val['idle'] = int(m.group(2))
        except:
            self.conn.close()
            try:
                self.conn.connect()
            except:
		pass
            self.val['hits'] = -1
            self.val['busy'] = -1
            self.val['idle'] = -1

# vim:ts=4:sw=4:et
