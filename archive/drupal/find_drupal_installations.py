#!/usr/bin/env python3

# from pip: pip install pyCLI (>=2.0.3) see requirements.txt
import cli.log
import io
import os.path
# debian package: python3-spur
import spur
# debian package: python3-yaml
import yaml


@cli.log.LoggingApp
def find_drupal_installations(app):
    r = dict()
    with io.open(app.params.config_file, 'r') as f:
        app.log.info('Loaded servers from {}'.format(app.params.config_file))
        c = yaml.load(f)
        for server in c['servers'].keys():
            if app.params.hosts is not None and server not in app.params.hosts:
                app.log.debug(
                    'Skipping defined server {}: not in selected hosts'.format(server)
                )
                continue
            for l in c['servers'][server]['search_locations']:
                user = l['user']
                r["{}@{}".format(user, server)] = []
                for directory in l['directories']:
                    a = search_remote(user, server, directory, app.log)
                    r["{}@{}".format(user, server)].extend(a)
    if app.params.output_file:
        app.log.debug('Trying to write output to {}'.
                      format(app.params.output_file))
        with io.open(app.params.output_file, 'w') as f:
            yaml.dump(r, f)
    else:
        print(yaml.dump(r))

def search_remote(user, server, directory, log):
    """
    Returns a list of [{directory: 'xx', 'version': 'x.xx'}] drupal installations
    or an empty list
    """
    log.info('About to attempt connection by ssh: {}@{}'.format(
        user, server))
    shell = spur.SshShell(hostname = server, username = user)
    drupals = []
    with shell:
        log.info('Searching for CHANGELOG.txt files in {} on {}'.
                 format(directory, server))
        try:
            result = shell.run(['find', directory, '-name', 'CHANGELOG.txt',
                                '-and', '-not', '-path', '*/sites/*',
                                '-and', '-not', '-path', '*/profiles/*',
                                # cheaty, but shell.run doesn't seem to glob
                                # directories like '/var/aegir/hostmaster-*'
                                # which works normally via sh / bash.
                                '-and', '-not', '-path', '/var/aegir/backups/*'],
                               allow_error = True)
        except Exception as e:
            log.warn('Failed to run find on {}@{}:{} : {}'.format(
                user, server, directory, e))
            return []
        changelogs = result.output.decode().split("\n")
        for changelog in changelogs:
            if not changelog:
                continue
            r = get_remote_changelog_version(shell, changelog, log)
            if r is not None:
                drupals.append(r)
    return drupals

def get_remote_changelog_version(shell, changelog, log):
    # Check Drupal 6 / Drupal 7
    r = shell.run(['grep', '-E', '^Drupal [0-9]\.[0-9]{2}. [0-9]{4}-[0-9]{2}-[0-9]{2}',
                   changelog], allow_error = True)
    log.debug('Trying to detect if {} is Drupal changelog file'.format(changelog))
    if r.output:
        l = r.output.decode().split("\n")
        # only take first entry
        v = l[0].split(',')[0].split(' ')[1]
        log.info('Detected Drupal version {} from {}'.format(v, changelog))
        return {
            'directory': os.path.dirname(changelog),
            'version': v,
            }
    # Check Drupal 8
    f = os.path.join(os.path.dirname(changelog), 'lib', 'Drupal.php')
    r = shell.run(['grep', '-E', "const VERSION = '[0-9].*", f],
                  allow_error = True)
    if r.output:
        v = r.output.decode().split("'")[1]
        log.info('Detected Drupal version {} from {}'.format(v, changelog))
        return {
            'directory':
              os.path.abspath(os.path.join(os.path.dirname(changelog), '..')),
            'version': v,
        }
    log.debug('No drupal version detected from {}'.format(changelog))
    return None

find_drupal_installations.add_param('-c', '--config', default = 'servers.yaml',
                                    action = 'store', dest = 'config_file',
                                    help = 'Configuration file for remote servers. Default: servers.yaml')
find_drupal_installations.add_param('-o', '--output', default = None,
                                    action = 'store', dest = 'output_file',
                                    help = 'Output the results (yaml) to the given file.')
find_drupal_installations.add_param('-d', '--hostname',
                                    action = 'append', dest = 'hosts')
if __name__ == '__main__':
    find_drupal_installations.run()
