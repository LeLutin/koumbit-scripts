#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
EditedSystemPages - list system pages that has been edited in this wiki.

@copyright: 2004 Nir Soffer <nirs@freeshell.org>
@license: GNU GPL, see COPYING for details.
"""

import os.path, sys, shutil, urllib

wikipath = '/var/alternc/html/r/root/moinmoin'
sys.path.append(wikipath);

wikipath = '.'
sys.path.append(wikipath);

from MoinMoin import wikiutil
from MoinMoin.wikiutil import unquoteWikiname
from MoinMoin.Page import Page
from MoinMoin.request import RequestCLI
from migutil import opj, listdir, move_file

dir_from = 'data'
dir_to   = 'underlay.backup'

os.mkdir(dir_to)

def renderInPage(request):
    """
    """        
    # Get page list for current user (use this as admin), filter
    # pages that are both underlay and standard pages.
    pages = request.rootpage.getPageList()
    print "f"
    for name in pages:
	    print ":" + name
    pages = [name for name in pages
             if pages[name].isStandardPage(includeDeleted=0) and
                 pages[name].isUnderlayPage(includeDeleted=0) or 1]
      
    # Format as numberd list, sorted by page name         
    pages.sort()
    for name in pages:
	    print name ,
    return ''

def check_page(page):
    """
    """
    if page.isUnderlayPage(includeDeleted=0) and page.isStandardPage(includeDeleted=0):
        rev_count = len(page.getRevList())
	if (rev_count > 1):
            print page.page_name.encode('utf-8') + ": ",
            if page.isUnderlayPage(includeDeleted=0):
                print "U",
            if page.isStandardPage(includeDeleted=0):
	        print "S",
	    print "revs: ", rev_count,
            print
	    # skip those pages
            safe = 0
    	else:
	    # page has only one revision and is both a standard and underlay page
	    safe = 1
    else:
        # don't remove non-underlay or underlay-only pages
    	safe = 0
    return safe

#request = RequestCLI('FrontPage')
print "pages with more than one revision, skipped:"
request = RequestCLI('FrontPage')

for fname in listdir(dir_from + "/pages"):
    page = Page(request, unquoteWikiname(fname))
    if check_page(page):
    	move_file(dir_from + "/pages/" + fname, dir_to + "/" + fname)

#renderInPage(request)
