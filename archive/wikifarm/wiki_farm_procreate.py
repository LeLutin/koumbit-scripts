#! /usr/bin/python
# -*- coding: latin-1 -*-

# this script assumes a few things
# 1- the wikifarm directory is setup as follows
#    - a config directory contains the config files, namely
#      - apache.conf, which contains the rewrite rules
#      - wikilist.py, which contains *only* the wikis variable
#      - template.py, a template for the wiki config, that will have WikiName setup as a site_name
#    - a wikis directory containing all the wikis
# 2- the wikifarm is setup using fast cgi and the moin.fcg file is in server/moin.fcg
# 3- we are root (although only the chown and the apachectl might fail here if other perms are right)

# TODO:
# This script is a bit of an ugly hack
#
# It should
#  1. do error checks on open and other syscalls
#  2. be split into functions
#  3. watch for dupes in apache.conf

def warn(str):
	sys.stderr.write(str + '\n')

def maybe_print(str):
	global options
	if options.verbose:
		sys.stderr.write(str + '\n')
	

from os import makedirs, system, chdir, symlink
from shutil import copytree

from optparse import OptionParser
import sys

parser = OptionParser(usage="usage: %prog [options] -w NAME")

parser.add_option('-n', '--dry-run', dest="dryrun", help="don't do anything, just print out the actual actions that would be performed. Implies --verbose.", action="store_true", default=False)
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="print what is being done to standard output")
parser.add_option("-q", "--quiet", action="store_false", dest="verbose", help="print what is being done to standard output")
parser.add_option('-w', '--wikiname', dest="shortname", help="the short name of this wiki, used in the url and the name of its configuration file", metavar="NAME")
parser.add_option('-s', '--sitename', dest="sitename", help="the full name of the wiki, will be shown in all wiki pages (defaults to NAME)", metavar="WikiName")
parser.add_option('-t', '--hostname', dest="hostname", help="the hostname under which this wiki should be served (default %default)", default="localhost")
parser.add_option('-u', '--urlprefix', dest="urlprefix", help="the prefix that gets appended after the hostname, needs a leading slash or empty (defaults to no prefix)", default="")
parser.add_option('-x', '--urlsuffix', dest="urlsuffix", help="a string that gets appended after the prefix (default /wikiname)")
parser.add_option('-p', '--prefix', dest="prefix", help="the prefix under which moinmoin is installed (default %default)", default="/usr/local")
parser.add_option('-r', '--server', dest="server", help="the server script to configure (default %default)", default="moin.fcg")
parser.add_option('-l', '--location', dest="wikifarm", help="the wikifarm base (defaults to PREFIX/www/wikifarm)")
parser.add_option('-g', '--plugindir', dest="plugindir", help="the location of the farm plugins, relative to the wikis data directories (default %default)", default="../../../plugin")
parser.add_option('-i', '--virtual', dest="virtual", action="store_true", help="create a virtual host for this wiki, disables the rewrite rule for the main wikifarm (default %default)", default=False)

global options
(options, args) = parser.parse_args()
if not options.shortname:
	parser.print_help()
	print "--wikiname (-w) is mandatory"
	sys.exit()

if not options.sitename:
	options.sitename = options.shortname

if options.dryrun and options.verbose != False:
	options.verbose = True

# shorthands of some often used options, XXX: should probably be removed
urlprefix = options.urlprefix

if options.urlsuffix == None:
	urlprefix += '/' + options.shortname
else:
	urlprefix += options.urlsuffix

# leading / is important
if len(urlprefix) < 1 or urlprefix[0] != '/':
	urlprefix = '/' + urlprefix

moindata = options.prefix + '/share/moin/data'
if not options.wikifarm:
	options.wikifarm = options.prefix + "/www/wikifarm"
wikifarm = options.wikifarm

# this is expected to be a fcgi for now, changes need to be made below otherwise
cgi_loc = wikifarm + '/server/' + options.server

import sys
# this is where the config files should sit
sys.path.append('config')
chdir(wikifarm)

from wikilist import wikis
# check if the wiki already exists
for wiki in wikis:
        if wiki[0] == options.shortname:
                raise NameError, 'This wiki already exists!'

# check if template contains the actual replacement variable
file = 'config/template.py'
conf = open(file, 'r')
# this variable is used later too!
template = conf.read()
conf.close()
if template.find('%%name%%') == -1:
	warn("WARNING: missing %s from template %s" % ('%%name%%', file))

# 1.1 add an apache rewrite rule for this wiki
# this was: RewriteRule ^/options.shortname(.*)$ /usr/local/www/wiki/wikifarm/server/moin.fcg$1 [type=application/x-httpd-fcgi,L]
# but now ScriptAlias is prefered (simpler):
# ScriptAlias /options.shortname /usr/local/www/wiki/wikifarm/server/moin.fcg
#line = 'RewriteRule ^' + urlprefix + '(.*)$ ' + cgi_loc + '$1 [type=application/x-httpd-fcgi,L]' + "\n"
if options.virtual:
	config = """
<VirtualHost *:80>
ServerAlias %s
Include %s/config/apache-common.conf
</VirtualHost>
""" % (options.hostname, wikifarm)
	conffile = options.prefix + '/apache2/Includes/' + options.shortname + '.conf'
	maybe_print("creating the following file: %s containing: %s" % (conffile, config))
else:
	line = 'ScriptAlias ' + urlprefix + ' ' + cgi_loc + "\n"
	file = 'config/apache.conf'
	maybe_print("adding to %s: %s" % (file, line))
	if not options.dryrun:
		conf = open(file, 'a')
		conf.write(line)
		conf.close()

# 1.2 add the pattern for this wiki in wikilist.py
# ("options.shortname",    r"^wikifarm.koumbit.net/options.shortname.*$"),
# it is not added at the end of the array, because that item is the biggest priority
wikis.insert(0, (options.shortname, r"^" + options.hostname + urlprefix + '.*$')),
maybe_print("adding %s to wikilist" % repr(wikis[0]))
if not options.dryrun:
	conf = open('config/wikilist.py', 'w')
	conf.write("# automatically edited by a script, only this variable will remain on next edit\n")
	conf.write('wikis = ' + repr(wikis) + "\n")
	conf.close()

# 1.3 create a config based on the template
maybe_print("generating a decent config")
if not options.dryrun:
	conf = open('config/' + options.shortname + '.py', 'w')
	conf.write(template.replace('WikiName', options.sitename).replace('%%name%%', options.shortname))
	conf.close()

# 1.4 copy datafiles (mkdir wikis/shortname && cp -R share/moin/data wikis/shortname/data)
dest = 'wikis/' + options.shortname
maybe_print("copying vanilla from %s to %s" % (moindata, dest))
if not options.dryrun:
	makedirs(dest)
	copytree(moindata, dest + '/data')

# 1.5 fix perms
maybe_print("fixing ownership on %s" % dest + '/data')
if not options.dryrun:
	system('chown -R www:www %s' % dest + '/data')

if options.plugindir:
	maybe_print("symlinking plugin directory")
	if not options.dryrun:
		system('rm -rf %s' % dest + '/data/plugin')
		symlink(options.plugindir, dest + '/data/plugin')

# restart apache
maybe_print("trying to restart apache")
if not options.dryrun:
	system('apachectl graceful')
