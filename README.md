Les packages koumbit-scripts-* sont gérés par l'équipe infrastructure de
Koumbit et contiennent des scripts utiles à l'administration de serveurs de
différents types.

De la documentation online est disponible avec des commandes pour comment faire
une release et installer les packages:

https://wiki.koumbit.net/ComitéSysAdmin/Scripts

Les scripts sont instalés dans /opt/bin, et donc ce répertoire doit être ajouté
à votre $PATH, ou bien doivent être appelés avec leur chemin absolu. La raison
pour ceci est que nous voulons éviter tout risque de conflit avec des fichiers
de packages venant des archives officielles de debian.

# Installer

Pour pouvoir utiliser les packages koumbit-scripts-*, on doit d'abord ajouter
une source à apt. Cette source est normalement installée automatiquement par
puppet mais pour la créer manuellement, voici à quoi son contenu ressemble:

    deb http://debian.koumbit.net/debian stable main

Chaque package contient des scripts qui sont utile pour un type de service. Si
on veut par exemple installer les scripts qui servent à l'administration de
courriels, on peut utiliser la commande suivante sur un serveur:

    apt install koumbit-scripts-mail

# Contribuer

Le code de ces scripts et les fichiers pour leur packaging sont tous dans le
même repository git. Vous pouvez trouver le code dans le projet redmine:

https://redmine.koumbit.net/projects/koumbit-scripts/repository

# Disposition des fichiers

Les fichiers sont disposés dans le repository dans différents répertoires pour
faciliter la gestion, de la façon suivante:

 * aegir, backup, mail, misc, monitoring, payscript, vps
   * chaque répertoire correspond au package koumbit-scripts-$nomdereprtoire
 * alternc, alternc_dstat
   * Ces deux répertoires sont le package koumbit-scripts-alternc.
     alternc_dstat contient spécifiquement un plugin pour dstat alors
     qu'alternc contient les scripts qui sont déployés dans /opt/bin
 * debian
   * fichiers de packaging. le paquet source koumbit-scripts est un package
     debian "natif" puisqu'il contient le répertoire debian direct dans le
     même repository que le code source.
 * Notez qu'il n'y a plus rien dans la racine: les scripts sont installés
   par domaine. Il ne sert à rien de mettre un fichier dans la racine:
   il ne sera pas installé !

# Créer un nouvelle release et publier les package sur l'archive de Koumbit

Les commandes exactes sont documentées avec plus d'explications dans la page
wiki:

https://wiki.koumbit.net/ComitéSysAdmin/Scripts

On commence par créer une nouvelle version des packages grâce à `dch -i`, puis
on teste la création des packages localement avec pbuilder ou sbuild. Ensuite
on finalise la release avec `dch -r`.

Pour choisir le numéro de version: on veut normalement suivre les consignes de
versionnement sémantique:

http://semver.org/

Ensuite on doit créer un tag avec git qui a le nom de la version qui est
nouvellement releasée. Et on pousse le code et le tag vers redmine.

Jenkins prendra automatiquement en charge la création des packages et leur
publication dans l'archive de Koumbit sous la version "unstable". On peut alors
tester que les packages fonctionnent bien lors de l'installation.

On peut ensuite migrer les packages à la version testing, puis stable avec la
commande `reprepro`.

