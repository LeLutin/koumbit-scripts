#!/bin/sh

# Genere une liste des installations de Joomla et leurs versions

ALTERNCDIR=/var/alternc/html/
if [ -n "$1" ] ; then
   ALTERNCDIR="$1"
fi
find $ALTERNCDIR -name CHANGELOG.php | while read file
do
        ISCOMPONENT=`echo $file | egrep '/(components|plugins)/'`

        # skip CHANGELOG.php files in components directory
        if [ -z "$ISCOMPONENT" ]; then
               BASE=`dirname "$file"`
               CONF="$BASE/configuration.php"

               if [ -f "$CONF" ]; then
                       if [ -x /usr/bin/alternc_get_path ] ; then
                               extra=`/usr/bin/alternc_get_path "$file" | tail -1`
                              if [ -z "$extra" ]; then
                                       extra="-\t-\t-"
                               fi
                       else
                               # check where the domain points to
                               URL=`grep live_site "$CONF" | sed 's/^.*live_site *= *//' | sed "s/['\";\r]//g"`
                               HOST=`echo "$URL" | sed -e 's#http://##' | sed -e 's#/.*$##'`
                               if ! [ -z "$HOST" ]; then
                                       IP=`host $HOST | awk '{ print $NF }'`
                               fi
                               extra="$URL($IP)\t-\t-"
                       fi

                       joomla=`grep -- '----------' "$file" | grep 'Releas' | head -1 | cut -f 2 -d ' '`
                       printf "$joomla\t$extra\t$file\n"
                fi
        fi
done
