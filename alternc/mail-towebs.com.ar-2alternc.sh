#! /bin/sh -x

# Inspirado del script vpopmail2alternc, contribucion de Anarcat.  
# Adaptado por SeBas.
# 
# La idea es de tener scripts que nos permiten migrar las casillas de 
# correo de una cuenta de towebs.com.ar a una cuenta AlternC
#
# 1) Desde la cuenta de towebs, bajar por ftp el directorio .mail que contiene 
#    las casillas de correo.
# 2) Generar la lista de cuenta a importar con ... en un archivo
#    usr_list.txt 
# 3) Crear las cuentas con mail_doaddfromfile.php 
# 4) Migrar el contenido de .mail/$usuario a /var/alternc/mail/... con
#    este script.
#
# NB: Esta pensado para migrar un solo dominio con multiples usuarios.
#
# 
# * Estamos en el repertorio que contiene .mail/...
# * Llamar el script con un argumento que es el dominio.
#   ./mail-towebs.com.ar2alternc.sh yo.org.ar   

cd .mail/
    for user in *
    do
      u=`printf %1.1s $user`
#      echo $user
      dest=:/var/alternc/mail/$u/$user\_$1/Maildir/
      echo $dest
#      if [ -x $dest ]; then
        # la mailbox existe d�j�, on la tue?
        # rm -rf $dest
#      fi;
      # mv $user $dest
    done

