#! /bin/sh -x

# migration des mails de vpopmail ? alternc
#
# il reste un probl?me dans le cas o? la mailboxe existe d?j?, ce qui va probablement toujours ?tre le cas:
# on ne peut pas fusionner les deux mailboxes, alors on efface l'ancienne?
# on pourrait faire une mini-fonction pour v?rifier qu'elle est vide.
#
# on assume que nous d?marrons dans le r?pertoire "domains" de vpopmail, que nous pouvons d?truire ? souhait
#
# ce r?pertoire peut-?tre copi? de la sorte:
# rsync -vaz old_server:/usr/local/vpopmail/domains/ old_stock/
# cd old_stock

for dom in *
do
  (cd $dom
    for user in *
    do
      u=`printf %1.1s $user`
      dest=/var/alternc/mail/$u/$user_$dom/Maildir/
      if [ -x $dest ]; then
        # la mailbox existe d?j?, on la tue?
        # rm -rf $dest
        :
      fi
      mv $user $dest
    done
  )
done
