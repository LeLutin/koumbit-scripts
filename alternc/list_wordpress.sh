#! /bin/sh

# This script looks for Wordpress installs in a given directory structure

# The output is a tab-seperated table of:
# directory     wordpress_version
# Explanations:
# - directory: the detected Drupal "root" (the path to node.module with modes/.*/node.module stripped)
# - wordpress_version: the x.x.x version found after the "Drupal" string, with -cvs appended if the first line of the CHANGELOG.txt has a "x.x.x" instead of a proper version. Can also be - if no CHANGELOG.txt is found or ? if it doesn't look like a Drupal CHANGELOG.txt

# It is expected that the wp-includes/version.php contains $wp_version = 'x.x.x';

# It is recommended that it is ran like this on an AlternC directory structure:
# ls -d /var/alternc/html/* | while read base ; do /root/bin/list_wordpress.sh $base ; done | /root/bin/email_from_path  | sed 's#/var/alternc/html/##g'

echo starting search on "$@" >&2
nice find "$@" -follow -depth -type f -a -name version.php | while read file
do
    echo "examining $file" >&2
    root=$(echo "$file" | sed 's#wp-includes/version.php##;s#/$##')
    root=$(readlink -f $root)
    if [ ! -d "$root" ] ; then
        continue
    fi
    if [ -f "$file" ]; then
        wp_version=$(cat "$file" | grep '$wp_version' | grep -o "'.*'" | tr -d "'")
        printf "$root\t$wp_version\n"
    else
        printf "$root\t-\n"
    fi
done
