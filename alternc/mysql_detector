#! /bin/sh -x

# this scripts tries to look for bad "host" strings in various CMS
# a bad "host string" is "localhost", as we're migrating to mysql.koumbit.net
# the way it does this is to find all scripts (defined as files named *.inc or *.php3..4) that have the sctring mysql_connect (and friends)
# it then uses heuristics to guess the CMS and find the configuration file to see if the right hostname is setup
# this has the obvious defect of requiring a bit of work for each CMS

# Todo
# * tiki?
# * egroupware?  -- non, les (deux) installations (connues) ont été migrées
# * mantis? -- lesquels? j'ai fait cmaq/koumbit fuckit then
# * phpwiki? -- fuckit

# simple usage
usage() {
	cat <<EOF
$0 [-n] <path>
	-n: dry run
	path: where to look

This script looks in the given directory and will try to change the CMS configuration to use "mysql.koumbit.net" instead of "localhost".

Supported CMS: SPIP, Drupal, Gallery2, Phpbb and Wordpress
EOF
	
}

# parse commandline
args=`getopt nh $*`
set -- $args

for i
do
  	case "$i" in
  	      -n) shift; dryrun=1;;
  	      -h) shift; usage; exit;;
  	      --) shift; break;;
	esac
done

# sole argument is to path to look into
path=$1

status() {
	[ -z "$dryrun" ] && echo fixed: $*
	[ ! -z "$dryrun" ] && echo needs fixing: $*
}

# define the pattern for drupal
DRUPAL_PATTERN='database.mysql.inc.*mysql_connect('

## ./includes/install.mysql.inc: $connection = @mysql_connect($url['host'], $url['user'], $url['pass'], TRUE, 2);
# this is a special pattern that ignores other drupals in the way
DRUPAL_IGNORED_PATTERN='includes/install.mysql.inc.*mysql_connect'
# find the base of the drupal
find_drupal_base() {
	echo $1 | sed 's#^\(.*\)/includes.*$#\1#'
}

# ./ecrire/inc_db_mysql.php3:     $spip_mysql_link = @mysql_connect($host, $login, $pass);
SPIP_PATTERN='db_mysql.*mysql_connect'
find_spip_base() {
	echo $1 | sed 's#^\(.*\)/ecrire/.*$#\1#'
}

# SPIP 1.9.x
# ./ecrire/base/db_mysql.php:     $spip_mysql_link = @mysql_connect($host, $login, $pass);
SPIP19_PATTERN='ecrire/base/db_mysql.*mysql_connect'
find_spip19_base() {
	echo $1 | sed 's#^\(.*\)/ecrire/base/.*$#\1#'
}
SPIP_IGNORED_PATTERN='ecrire/\(inc\|exec/\)\?install.*mysql_connect'

# wp-includes/wp-db.php:		$this->dbh = @mysql_connect($dbhost, $dbuser, $dbpassword
WORDPRESS_PATTERN='wp-db.php.*mysql_connect'
find_wp_base() {
	echo $1 | sed 's#^\(.*\)/wp-includes/.*$#\1#'
}

# ./gallery-2.0.1/lib/adodb/drivers/adodb-mysql.inc.php: $this->_connectionID = mysql_connect($argHostname,$argUsername,$argPassword, 
GALLERY_PATTERN='lib/adodb/drivers/adodb-mysql.inc.php.*connectionID.*mysql_p\?connect'
find_gallery_base() {
	echo $1 | sed 's#^\(.*\)/lib/adodb/.*$#\1#'
}

# ligne d'exemple phpbb
# ./db/mysql4.php:                $this->db_connect_id = ($this->persistency) ? mysql_pconnect($this->server, $this->user, $this->password) : mysql_cnnnect($this->server, $this->user, $this->password);
# pattern phpbb
PHPBB_PATTERN='db/mysql4\?\.php.*db_connect_id.*mysql_p\?connect'
find_phpbb_base() {
	echo $1 | sed 's#^\(.*\)/db/.*#\1#'
}

# main loop: find all files like .php (3/4) and .inc, grep for mysql connect strings
find $path -iregex '.*\.php[34]?' -o -iname '*.inc' -print0 | xargs -0 -n 100 egrep 'mysql_p?connect[[:space:]]*\(' | while read connect
do
	# this could probably be a case, but we started with regexp..
	if echo $connect | grep -q "$DRUPAL_PATTERN"; then
		base=`find_drupal_base "$connect"`
		# $db_url = 'mysql://decisions:XXXXXXX@localhost/decisions';
		for file in $base/sites/*/settings.php $base/includes/conf.php
		do
			if [ -e $file ] && grep -q '\$db_url.*localhost' $file ; then
				status drupal in $base
				[ -z "$dryrun" ] && sed -i -e '/\$db_url.*localhost/s/localhost/mysql.koumbit.net/' $file
			fi
		done
	elif echo $connect | grep -q "$SPIP_PATTERN"; then
		base=`find_spip_base "$connect"`
		# ... spip_connect_db('localhost ...
		if [ -e $base/ecrire/inc_connect.php* ]; then
			if grep -q 'spip_connect_db.*localhost' $base/ecrire/inc_connect.php* ; then
				status spip in $base
				[ -z "$dryrun" ] && sed -i -e '/spip_connect_db.*localhost/s/localhost/mysql.koumbit.net/' $base/ecrire/inc_connect.php*
			fi
		fi
	elif echo $connect | grep -q "$SPIP19_PATTERN"; then
		base=`find_spip19_base "$connect"`
		# ... spip_connect_db('localhost ...
		if [ -e $base/ecrire/inc_connect.php* ]; then
			if grep -q 'spip_connect_db.*localhost' $base/ecrire/inc_connect.php* ; then
				status spip in $base
				[ -z "$dryrun" ] && sed -i -e '/spip_connect_db.*localhost/s/localhost/mysql.koumbit.net/' $base/ecrire/inc_connect.php*
			fi
		fi
	elif echo $connect | grep -q "$WORDPRESS_PATTERN"; then
		base=`find_wp_base "$connect"`
		# wp-config.php:define('DB_HOST', 'localhost');
		if [ -e $base/wp-config.php ]; then
			if grep -q 'DB_HOST.*localhost' $base/wp-config.php ; then
				status wordpress in $base
				[ -z "$dryrun" ] && sed -i -e '/DB_HOST.*localhost/s/localhost/mysql.koumbit.net/' $base/wp-config.php
			fi
		fi
	elif echo $connect | grep -q "$GALLERY_PATTERN"; then
		base=`find_gallery_base "$connect"`
		# $storeConfig['hostname'] = 'mysql.koumbit.net';
		if [ -e $base/config.php ]; then
			if grep -q 'storeConfig.*hostname.*localhost' $base/config.php ; then
				status gallery2 in $base
				[ -z "$dryrun" ] && sed -i -e '/storeConfig.*hostname.*localhost/s/localhost/mysql.koumbit.net/' $base/config.php
			fi
		fi # autrement gallery est pas installé
	elif echo $connect | grep -q "$PHPBB_PATTERN"; then
		base=`find_phpbb_base "$connect"`
		# $dbhost = 'localhost';
		if [ -e $base/config.php ] ; then
			if grep -q '\$dbhost.*localhost' $base/config.php ; then
				status phpbb in $base
				[ -z "$dryrun" ] && sed -i -e '/\$dbhost.*localhost/s/localhost/mysql.koumbit.net/' $base/config.php
			fi
		fi
	elif echo $connect | grep -q "$DRUPAL_IGNORED_PATTERN"; then
		# nothing
		true
	elif echo $connect | grep -q "$SPIP_IGNORED_PATTERN"; then
		# nothing
		true
	else
		echo `dirname "$connect"` is from an unknown CMS, good luck with it
	fi
done
