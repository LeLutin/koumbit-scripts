#!/bin/sh

# [ML] 2009-01-22 Script qui synchronise une liste de membres AlternC avec une liste Mailman
# based on civicrm_mailman_sync.php

MYSQL="mysql  --defaults-file=/etc/mysql/alternc.cnf -N -B"
MMLIST="hag-koumbit.org"

# MMPARAMS="-n -w=yes -g=no -a=yes" # simulate, welcome yes, goodbye no, admin notify yes
MMPARAMS="-w n -a y"            # welcome no, admin notify yes

# MMADD="/usr/sbin/sync_members $MMPARAMS"
MMADD="/var/lib/mailman/bin/add_members $MMPARAMS"

# Add the Mailman sync_members parameters here
# see http://www.tin.org/bin/man.cgi?section=8&topic=sync_members for syntax

TEMP_FILE=`mktemp -t alternc_mailman_sync.XXXXXXXXX`

if [ -z "$TEMP_FILE" -o ! -f "$TEMP_FILE" ]; then
        exit 3
fi

$MYSQL -e 'select mail from membres where subscribed = 0 ' > $TEMP_FILE

# count if there is anyone to subscribe
COUNT=`stat -c "%b" $TEMP_FILE`

if [ $COUNT -ne 0 ]; then
	$MMADD -r $TEMP_FILE $MMLIST
	while read member
	do
		$MYSQL -e "update membres set subscribed = 1 where mail = '$member';"
	done < $TEMP_FILE
fi

rm $TEMP_FILE
