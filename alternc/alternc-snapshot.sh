#!/bin/sh

BACKUPDIR=/backup

# unreadble for others
umask 027

. /etc/alternc/alternc.conf
MYSQLUSER=$dbuser
MYSQLPASS=$dbpwd

date=`date +%Y%m%d`

##
## garder 4 jours de backup
## eliminer les anciens
##
oldbackups=`ls ${BACKUPDIR} | sort -nr | tail +5`
for ob in ${oldbackups}; do
	rm -rf ${BACKUPDIR}/${ob}
done

##
## creer notre repetoire pour 
## le backup d'aujourdh'ui
##
BACKUPS=${BACKUPDIR}/${date}
rm -rf ${BACKUPS}; mkdir ${BACKUPS}

mkdir ${BACKUPS}/databases
mkdir ${BACKUPS}/files

##
## backup des db mysql
##
databases=`echo 'show databases' | mysql -u ${MYSQLUSER} -p${MYSQLPASS} system | tail +2`
for db in $databases; do
	mysqldump -u ${MYSQLUSER} -p${MYSQLPASS} ${db} | gzip -9c - > ${BACKUPS}/databases/${db}.dump.gz
done

BACKUPS=${BACKUPS}/files
##
## backup des donnees et configuration
##
cd /
tar -czf ${BACKUPS}/etc.tar.gz etc
tar -czf ${BACKUPS}/var-alternc.tar.gz --exclude 'var/alternc/db' var/alternc
tar -czf ${BACKUPS}/var-lib-mailman.tar.gz var/lib/mailman
