#!/bin/bash
# Update domain next-gen by fufroma

for CONFIG_FILE in \
      /etc/alternc/local.sh \
      /usr/lib/alternc/functions.sh \
      /usr/lib/alternc/functions_hosting.sh \
      /usr/lib/alternc/functions_dns.sh
  do
    if [ ! -r "$CONFIG_FILE" ]; then
        echo "Can't access $CONFIG_FILE."
        exit 1
    fi
    . "$CONFIG_FILE"
done


# Some vars
umask 022
LOCK_FILE="/usr/share/alternc/panel/cron-fixperms.lock" # FIXME doesn't seem clean to be here
THROUGH_FILE="/usr/share/alternc/panel/memberlist" # FIXME doesn't seem clean to be here
OLDIFS="$IFS"
NEWIFS=" "
TOUNFUCK=" "
TOUNFUCK="$(mktemp /tmp/alternc_reload_web-tounfuck.XXXX)"
RELOAD_WEB="$(mktemp /tmp/alternc_reload_web.XXXX)"
B="µµ§§" # Strange letters to make split in query

# Somes check before start operations
if [ `id -u` -ne 0 ]; then
    echo "must be launched as root"
elif [ -z "$DEFAULT_MX" -o -z "$PUBLIC_IP" ]; then
    echo "Bad configuration. Please use: dpkg-reconfigure alternc"
elif [ -f "$LOCK_FILE" ]; then
    process=$(ps f -p `cat "$LOCK_FILE"|tail -1`|tail -1|awk '{print $NF;}')
    if [ "$(basename $process)" = "$(basename "$0")" ] ; then
      echo "last cron unfinished or stale lock file ($LOCK_FILE)."
    else
      rm "$LOCK_FILE"
    fi
fi

# We lock the application
echo $$ > "$LOCK_FILE"

# For domains we want to delete completely, make sure all the tags are all right
# set sub_domaines.web_action = delete where domaines.dns_action = DELETE

for member in $(mysql_query "select login from membres") ; 
do
    if [ "$(grep ^$member$ ${THROUGH_FILE} | uniq)" = "$member" ] ; 
    then 
       echo "skipping $member, already done" ; 
    else
       [ $(date "+%H%M") -gt 700 ] && {
         echo "its past 7AM ! I'm going home !";
         exit 1;
       }
       echo "doing $member - working hard " ; 
       /usr/lib/alternc/fixperms.sh -l $member 
       echo $member >> $THROUGH_FILE
    fi
done

