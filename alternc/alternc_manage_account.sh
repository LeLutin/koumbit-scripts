#!/bin/bash

# Management actions against AlternC accounts
# Expects AlternC's principal database to be accessible through mysql command line client
#
# Environment variables checked:
# ALTERNC_DBHOST
# ALTERNC_DBUSER
# ALTERNC_DBNAME
#
# DEBUG : any non-empty value makes the script show debug information

debug() {
    if [ -n "$DEBUG" ] ; then
        echo "$*"
    fi
}

if [ -z "$ALTERNC_DBHOST" ] ; then
    ALTERNC_DBHOST=''
fi
if [ -z "$ALTERNC_DBUSER" ] ; then
    ALTERNC_DBUSER=''
fi
if [ -z "$ALTERNC_DBNAME" ] ; then
    ALTERNC_DBNAME=''
fi

MYSQL_ARGS=''
if [ -n "$ALTERNC_DBUSER" ] ; then
    MYSQL_ARGS="$MYSQL_ARGS -u $ALTERNC_DBUSER"
fi
if [ -n "$ALTERNC_DBHOST" ] ; then
    MYSQL_ARGS="$MYSQL_ARGS -h $ALTERNC_DBHOST"
fi
if [ -n "$ALTERNC_DBNAME" ] ; then
    MYSQL_ARGS="$MYSQL_ARGS -D $ALTERNC_DBNAME"
fi
debug "MySQL arguments: $MYSQL_ARGS"

mysqlq() {
    debug "EXECUTING: mysql $MYSQL_ARGS -e \"$*\""
    mysql $MYSQL_ARGS -e "$*"
}


# Defaults
VERBOSE=''
ACCOUNT=''
ACTION=''
NOOP=''

# Parse args: http://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
#echo "Num args: $#"
while [[ $# -ge 1 ]]; do
  key="$1"
  #echo "key: $key"
  case $key in
      -v|--verbose)
          VERBOSE="true"
          ;;
      -l|--login)
          shift
          LOGIN="$1"
          ACCOUNT=$(mysql $MYSQL_ARGS --skip-column-names -e "SELECT uid FROM membres WHERE login='$LOGIN'")
          debug "Found account uid '$ACCOUNT' for login '$LOGIN'"
          if [[ -z "$ACCOUNT" ]]; then
              echo "error: Account name '$LOGIN' was not found" >&2
              exit 1
          fi
          if ! [[ "$ACCOUNT" =~ ^[0-9]+$ ]]; then
              echo "internal error: UID value that was extrapolated from login name is not an integer! login='$LOGIN' UID='$ACCOUNT'" >&2
              exit 1
          fi
          ;;
      -c|--account)
          shift
          ACCOUNT="$1"
          ;;
      -n|--noop)
          NOOP="true"
          ;;
      *)
          ACTION=$1
          debug "Action: $ACTION"
          shift
          break
  esac
  shift
done

usage() {
    echo "$0 [-v|--verbose] <-l ACCOUNT_LOGIN|-c ACCOUNT_ID> <ACTION>"
    echo -e "\t-l|--login ACCOUNT_LOGIN: The account name (the login field in \
the membres table).\n\t\tEither this or -c is required"
    echo -e "\t-c|--account ACCOUNT_ID: The numeric account id (the uid field in \
the membres table).\n\t\tEither this or -l is required"
    echo -e "\t[-v|--verbose]: Output more text"
    echo -e "\tACTION: Currently supported actions: enable | disable"
    echo -e "\t\tdisable: Disable user login, password change permission, \
and queue all domains\n\t\t\tfor deactivation."
    echo -e "\t\tenable: Enable user login, password change permission, \
and queue all domains\n\t\t\tfor activation."
}

if [ -z "$ACCOUNT" ] ; then
    echo "Account ID must be specifiied"
    usage
    exit 1
fi

if [[ "$ACTION" == 'enable' ]] ; then
    if [ $VERBOSE ] ; then
        echo "Account: "
        mysqlq "select uid, login, mail from membres where uid = $ACCOUNT"
        echo
        echo "The following domains will be enabled:"
        mysqlq "select concat_ws('.', sub, domaine) from sub_domaines where compte = $ACCOUNT"
        echo
        echo "The following email addresses will be enabled:"
        mysqlq "select concat_ws('@', a.address, d.domaine) from address a left join domaines d on a.domain_id=d.id left join membres m on d.compte=m.uid where m.uid=$ACCOUNT and a.enabled=0;"
    fi
    if [ -z "$NOOP" ] ; then
        echo "ENABLE!"
        mysqlq "update membres set enabled = 1, canpass = 1 where uid = $ACCOUNT"
        mysqlq "update sub_domaines set web_action = 'UPDATE', enable = 'ENABLE' where compte = $ACCOUNT"
        mysqlq "update address a left join domaines d on a.domain_id=d.id left join membres m on d.compte=m.uid set a.enabled=1 where m.uid=$ACCOUNT and a.enabled=0;"
    else
        echo "No-op, nothing done"
    fi
    exit 0
elif [[ $ACTION == 'disable' ]] ; then
    if [ "$VERBOSE" ] ; then
        echo "Account: "
        mysqlq "select uid, login, mail from membres where uid = $ACCOUNT"
        echo
        echo "The following domains will be disabled:"
        mysqlq "select concat_ws('.', sub, domaine) from sub_domaines where compte = $ACCOUNT"
        echo
        echo "The following email addresses will be disabled:"
        mysqlq "select concat_ws('@', a.address, d.domaine) from address a left join domaines d on a.domain_id=d.id left join membres m on d.compte=m.uid where m.uid=$ACCOUNT and a.enabled=1;"
    fi
    if [ -z "$NOOP" ] ; then
        echo "DISABLE!"
        mysqlq "update membres set enabled = 0, canpass = 0 where uid = $ACCOUNT"
        mysqlq "update sub_domaines set web_action = 'UPDATE', enable = 'DISABLE' where compte = $ACCOUNT"
        mysqlq "update address a left join domaines d on a.domain_id=d.id left join membres m on d.compte=m.uid set a.enabled=0 where m.uid=$ACCOUNT and a.enabled=1;"
    else
        echo "No-op, nothing done"
    fi
    exit 0
else
    echo "Unknown action $ACTION"
    usage
    exit 1
fi
