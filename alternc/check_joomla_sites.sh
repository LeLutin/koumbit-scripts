#!/bin/sh

# Verifie les versions des Joomla pour voir s'il y a des sites avec des failles connues

ALTERNCDIR=/var/alternc/html/

echo $0

if [ "X$1" = "Xcheck" ]; then
	# echo "Checking $2 ..."
	joomla=`grep -- '----------' "$2" | grep 'Releas' | head -1 | cut -f 2 -d ' '`
	# echo $joomla

	MAJORV=`echo $joomla | cut -f 1 -d '.'`
	MEDV=`echo $joomla   | cut -f 2 -d '.'`
	MINORV=`echo $joomla | cut -f 3 -d '.'`

	# On verifie Joomla <= 1.0.12 .. mais il y a aussi des 1.5.0 en beta qui peuvent etre vulnerables
	# C.f. RtTicket:11807
	if [ $MAJORV -eq 1 ]; then
		if [ $MEDV -eq 0 ]; then
			if [ $MINORV -le 12 ]; then
				# http://www.joomla.org/content/view/1843/74/
				echo "$2 : Joomla INSECURE .. version $joomla <= 1.0.12"
			fi
		else
			echo "$2 : Joomla VERIFY .. version $joomla"
		fi
	fi

	# On verifie Mambo < 4.6.3 .. quoique le dev de mambo est arrete', ne devrait pas etre utilise
	if [ $MAJORV -eq 4 ]; then
		# Mambo
		if [ $MEDV -lt 6 ]; then
			# http://mambo-code.org/gf/project/mambo/news/
			echo "$2 : Mambo INSECURE .. version $joomla < 4.6.3"
		elif [ $MINORV -lt 3 ]; then
			# http://mambo-code.org/gf/project/mambo/news/
			echo "$2 : Mambo INSECURE .. version $joomla < 4.6.3"
		fi
	fi
else
	find $ALTERNCDIR -name CHANGELOG.php | while read file
	do
		ISCOMPONENT=`echo $file | grep components`

		if [ -z "$ISCOMPONENT" ]; then
			$0 check "$file"
		fi
	done
fi

