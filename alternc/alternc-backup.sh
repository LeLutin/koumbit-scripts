#! /bin/sh

# backup user password will have to be stored in ~/.my.cnf
# ToDo: where is ~? Who will this be running as? root? www-data?

# ENV variables expected to be set:
# $username: the AlternC username
# $user_dbs: all the user's databases, in a format useable by mysqldump(8). might be possible to fetch from alternc
# $backup_host, $backup_user, $backup_path: see above.
# $TMP, a temporary location to store the database backup

backup_user=rsyncuser
backup_host=zoidberg.mine.nu
backup_path=incoming/216.46.21.45/
databases="cmaq cmaq_collectif cmaq_mantis cmaq_prod cmaq_tech"
username=cmaq
TMP=/tmp

mysqldump="mysqldump"
mysqlflags="-F -l -Q -u sys_backup --databases"

rsync="rsync"
rsync_flags="-az" 
ssh="ssh -p 222"

# no config below this line
u=`printf %1.1s $username`
user_home=/var/alternc/html/$u/$username

# parse command line
args=`getopt df $*`

if [ $? != 0 ]
then
    usage
    exit 2
fi
set -- $args

fflag=0
dflag=0

for i
do
  case "$i"
  in
      -d)
          dflag=1
          shift;;
      -f)
          fflag=1
          shift;;
      -h)
          usage;
          exit;;
      --)
          shift; break;;
  esac
done

if [ $dflag != 1 ]; then
    sql_dump=`mktemp $TMP/cmaq-db-dump-XXXXXX`
    trap "rm -f $sql_dump" 1 2 15

    chmod 600 $sql_dump

    $mysqldump $mysqlflags $databases >> $sql_dump
    files=$sql_dump
fi

if [ $fflag != 1 ]; then
    files="$files $user_home"
fi

if [ $fflag != 1 -o $dflag != 1 ]; then
    # compress the dump? will have to benchmark to see if it's better with rsync

    # we send the whole thing, which should include the sql dump
    $rsync $rsync_flags -e "$ssh" $files $backup_user@$backup_host:$backup_path
    rm -f $sql_dump
fi
