#!/usr/bin/php
<?php

/*
 $Id$
 ----------------------------------------------------------------------
 AlternC - Web Hosting System
 Copyright (C) 2002 by the AlternC Development Team.
 http://alternc.org/
 ----------------------------------------------------------------------
 Based on:
 Valentin Lacambre's web hosting softwares: http://altern.org/
 ----------------------------------------------------------------------
 LICENSE

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License (GPL)
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 To read the license please visit http://www.gnu.org/copyleft/gpl.html
 ----------------------------------------------------------------------
 Original Author of file: Resau Koumbit (Mathieu Lutfy)
 Purpose of file: Warn users who have exceeded their bandwidth quota
 ----------------------------------------------------------------------
*/

require_once("/var/alternc/bureau/class/config_nochk.php");

/* 
 * A few details:
 * - this script should run nightly, by cron
 * - it should send a warning when the user exceeds their bandwidth quota
 * - it should only warn once, but it should send a second warning when
 *   they have reached $quota + X.
 * - it should give alternc admins an option to CC/BCC another e-mail
 *   so that staff may monitor over-quotas and follow-up on billing.
 * - it should allow alternc admins to personalize the e-mail sent.
 */

// TODO
// - do not activate by default, add a variable to decide : { no warnings, warn admins, warn client } (stats_show_per_month.php + get_variable())
// - do not send a warning every night, only for some period

/*
 * Configuration variables (to move elsewhere?) FIXME
 */

$accountancy_mail = "comptes@rt.koumbit.net";
$accountancy_from = "Koumbit Comptabilite <$accountancy_mail>";
$warning_subject = "Koumbit: overquota for "; // account has exceeded its monthly bandwidth quota";
$url_info = "http://koumbit.org/hosting/overquota";
$template_directory = "/usr/share/alternc/templates";

// if this is true, no action will be taken
$simulate = FALSE;

// if this is set, all mails will be sent to this address:
// $override_email = "mlutfy@koumbit.org";
$override_email = "comptes@rt.koumbit.net";

// if this is TRUE, then the emails will be sent even if it's not the first day of overquota
$first_run = FALSE;

// Fetch a template with the text to send to the user
// the template is a PHP file with a function having the same name.
// EX: file warning_overquota_bandwidth.php, function warning_overquota_bandwidth($tokens).
// to provide a custom message, create a file with the same filename + '.custom' at the end,
// EX: file warning_overquota_bandwidth.php.custom, function warning_overquota_bandwidth($tokens).
function get_text_from_template($template, $tokens) {
	global $template_directory;

	$t = $template_directory . "/" . $template . '.php';

	if (file_exists($t . '.custom')) {
		$t = $t . '.custom';
	}

	if (file_exists($t)) {
		$f = 'alternc_get_template_' . $template;
		include_once($t);
		return $f($tokens);
	}

	die("Could not find template: $template");
}

// Taken from Drupal 6, except that we do not accept empty addresses
// http://api.drupal.org/api/function/valid_email_address/6
function is_valid_email_address($mail) {
	if (! $mail)
		return false;

	$user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
	$domain = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.?)+';
	$ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
	$ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';

	return preg_match("/^$user@($domain|(\[($ipv4|$ipv6)\]))$/", $mail);
}

function send_email($mail_to, $subject, $template, $tokens) {
	global $accountancy_mail;
	global $accountancy_from;
	global $override_email;

	if (! is_valid_email_address($mail_to)) {
		return FALSE;
	}

	if ($override_email) {
		$mail_to = $override_email;
	}

	$headers = "From: $accountancy_from\n".
		"MIME-Version: 1.0\n".
		"Content-Type: text/plain; charset=utf-8\n".
		"Content-Transfer-Encoding: 8bit\n";

	// encode the subjet if possible
	if(function_exists('mb_encode_mimeheader') AND @mb_internal_encoding('utf-8')) {
		$subject = mb_encode_mimeheader($subject, 'utf-8', 'Q');
	}

	// fetch the template "hi there, you're over quota"
	$text = get_text_from_template($template, $tokens);

	return mail($mail_to, $subject, $text, $headers);
}

function process_result($tokens) {
	global $err, $db, $cuid;
	global $simulate, $first_run, $warning_subject;

	$mail  = $tokens['%mail'];
	$login = $tokens['%login'];
	$uid   = $tokens['%uid'];

	// echo "Checking " . $tokens["%uid"] . ': ' . $tokens["%webusage"] . ' / ' . $tokens["%quota"] . ' -- ' . $tokens["%mail"] . ": ";

		// Check if a warning was already sent. For now, we send only
		// one warning, and it's up to people to monitor their stats so
		// that they do not go too much over-quota
		//
		// The logic is silly: if the user was over-quota yesterday, then
		// assume that a warning was sent.
		$q = "SELECT sum(h.size) as webusage
			FROM stat_http as h 
			WHERE YEAR(day) = YEAR(NOW()) 
			  AND MONTH(day) = MONTH(NOW())
			  AND DAY(day) < DAY(NOW())
			  AND h.uid = $uid";

		$db->query($q);
		$warning_already_sent = false;

		while ($db->next_record()) {
			if ($db->f('webusage') > $tokens['%quota']) {
				$warning_already_sent = true;
			}
		}

		if ($warning_already_sent && !$first_run) {
			echo "not new, not sending\n";
			return;
		} else {
			echo "new, sending\n";
		}

		// Get the list of domains and their bandwidth usage
		$q = "SELECT h.domain, sum(h.size) as webusage
			FROM stat_http as h 
			WHERE YEAR(day) = YEAR(NOW()) 
			  AND MONTH(day) = MONTH(NOW())
			  AND h.uid = $uid
			GROUP BY h.domain";
	
		$db->query($q);

		$tokens['%domains'] = '';
		while($db->next_record()) {
			$domain = $db->f('domain');
			$webusage = format_size($db->f('webusage'));

			$tokens['%domains'] .= $domain . " : ". $webusage . "\n";
		}

		// Send the e-mail to the user
		if (is_valid_email_address($mail)) {
			if ($simulate) {
				echo "not sending email to $mail ($uid/$login) because simulate is on\n";
				$result = true;
			} else {
				$result = send_email($mail, $warning_subject . $login, 'warning_overquota_bandwidth', $tokens);
			}

			if ($result !== TRUE) {
				echo "$uid: failed to send e-mail to $mail\n";
			}
		} else {
			echo "Account $uid ($login) has an invalid e-mail\n";
		}
}

function main() {
	global $err, $db, $cuid;
	global $accountancy_mail, $url_info;

	$hosting_tld = variable_get('hosting_tld');
	$month = 0;

	$over_quota = array();
	$results = array();

	$q = "SELECT h.uid, m.login, m.mail, sum(h.size) as webusage, q.total as quota, YEAR(NOW()) as year, MONTH(NOW()) as month
		FROM stat_http as h 
		LEFT JOIN quotas as q on (q.uid = h.uid and q.name = 'bw_web') 
		LEFT JOIN membres as m on (m.uid = h.uid) 
		WHERE YEAR(day) = YEAR(NOW()) 
		  AND MONTH(day) = MONTH(NOW())
		GROUP BY h.uid 
		HAVING webusage > total";

	$db->query($q);

	while($db->next_record()) {
		$tokens = array(
			'%uid' => $db->f("uid"),
			'%login' => $db->f("login"),
			'%mail' => $db->f("mail"),
			'%webusage' => format_size($db->f("webusage")),
			'%quota' => format_size($db->f("quota")),
			'%contactmail' => $accountancy_mail,
			'%url_info' => $url_info,
			'%hosting_tld' => $hosting_tld,
			'%year' => $db->f("year"),
			'%month' => $db->f("month"),
		);
		$results[$db->f('uid')] = $tokens;
	}
	foreach ($results as $result) {
		process_result($result);
	}
}

main();
