#! /bin/sh

# This script looks for Drupal installs in a given directory structure

# The output is a tab-seperated table of:
# directory	drupal_version	date
# Explanations:
# - directory: the detected Drupal "root" (the path to node.module with modes/.*/node.module stripped)
# - drupal_version: the x.x.x version found after the "Drupal" string, with -cvs appended if the first line of the CHANGELOG.txt has a "x.x.x" instead of a proper version. Can also be - if no CHANGELOG.txt is found or ? if it doesn't look like a Drupal CHANGELOG.txt
# - date: the date of the release

# It is expected that the CHANGELOG.txt starts like this:
# 
# Drupal 4.7.3, 2006-08-02
# 
# or
# 
# Drupal x.x.x, xxxx-xx-xx
# 
# for cvs versions, in which cases the script will look for another version later in the file and append -cvs to it

# It is recommended that it is ran like this on an AlternC directory structure:
# ls -d /var/alternc/html/* | while read base ; do /root/bin/list_drupal.sh $base ; done | /root/bin/email_from_path  | sed 's#/var/alternc/html/##g'

# find all node.module files in the directory, which are a good indicator of a
# Drupal, since the CHANGELOG.txt can be safely removed (and it is also
# recommended)
echo starting search on "$@" >&2
nice find "$@" -follow -depth -type f -a -name node.module | while read file
do
	extra=""
	echo "examining $file" >&2
	# find the root of the drupal. we can't just remove modules/ because of 5.x
	drupal_root=`echo "$file" | sed 's#modules/.*node.module##;s#/$##'`
	# the CHANGELOG.txt file should be there
	changelog=$drupal_root/CHANGELOG.txt
	extra="$drupal_root"
	drupal_root=`readlink -f $drupal_root`
	if [ -f "$changelog" ]; then
		# the first line usually contains the version number, find it and clean it up
		first=`grep '^Drupal' "$changelog" | head -1`
		# CVS version, try to detect from which release anyways
		if echo "$first" | fgrep -q 'Drupal x.x.x' ; then
 			# match 2 "Drupal" lines, take the second
			first=`grep -m 2 '^Drupal' "$changelog" | tail -1`
			cvs="-cvs"
		fi
		if [ -z "$first" ]; then
			printf "$drupal_root\t?"
		else
			# cleanup the version: split the date from the version with a tab, remove the Drupal word
			first=`echo $first | sed "s/, /$cvs\t/;s/Drupal //"`
			# print as tab-seperated value
			printf "$drupal_root\t$first"
		fi
	else
		printf "$drupal_root\t-"
	fi
	printf "\t$extra\n"
done
