#!/usr/bin/perl 

use strict;

my ($mailname,$uid,$folder) = @ARGV;

if (!$mailname || !$uid || !$folder) {
    print "Simple script to add a folder to an existing mailbox.\n";
    print "Usage: <mailname> <uid> <folder>\n";
    exit(1);
}

$ENV{PATH} = "";
delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

if (!($mailname =~ /^([a-z0-9_\+\-][a-z0-9_\+\.\-]+\_[a-z0-9\.-]+)$/)) {
    die "Email is incorrect.";
}
$mailname=$1;
if (!($uid =~ /^([0-9]+)$/)) {
    die "uid is incorrect.";
}
$uid=$1;

$< = $>;
$( = $);

my $PTH="/var/alternc/mail/".substr($mailname,0,1)."/".$mailname;

my @todo=(
       $PTH."/Maildir/.".$folder,
       );

foreach(@todo) {
    system("/usr/bin/maildirmake '$_'");
    system("/bin/chown -R 33:'$uid' '$PTH'");
    system("/bin/chmod -R 0750 '$PTH'");
}

system("/bin/echo INBOX.".$folder." >> '/var/alternc/mail/".substr($mailname,0,1)."/".$mailname."/Maildir/courierimapsubscribed'");

0;

