#!/bin/bash
# written by sysreq (sebastien@koumbit.org), 08/10/07
#
# checks if apache's parent process (ran as root) is dead.
# if so, it kills the remaining processes and restarts apache.
#
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin

# First test
if [ `ps aux | grep -c '^root.*[0-9] /usr/sbin/apache2$'` -eq 0 ] || [ $1 = 'force' ]; then

  /etc/init.d/apache2 stop

  for i in `pidof apache2`;
  do
    kill -9 $i && echo sent KILL signal to apache process $i
  done

  /etc/init.d/apache2 start
  echo "Apache was restarted on $HOSTNAME"

fi

