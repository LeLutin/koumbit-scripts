#!/bin/sh

# Affiche les comptes over-quota
# Parametre: 0 = ce mois-ci, 1 = le mois dernier, etc.

# Le truc bizarre avec les mois c'est pour gerer le cas ou on est le 1er janvier
# et on veut gerer les quotas de decembre. Par contre, on ne gere pas les cas ou
# l'on veut obtenir les stats d'il y a plus de 12 mois.

MONTH=$1

if [ "x$MONTH" = "x" ]; then
	MONTH=1
fi

mysql --defaults-file=/etc/alternc/my.cnf \
	-e "SELECT h.uid, m.login, sum(h.size) / 1024 / 1024 / 1024 as webusage, q.total / 1024 / 1024 / 1024 as total, IF (MONTH(NOW()) - $MONTH < 1, YEAR(NOW()) - 1, YEAR(NOW())) as year, IF (MONTH(NOW()) - $MONTH < 1, 12 - $MONTH + MONTH(NOW()), MONTH(NOW()) - $MONTH ) as month \
		FROM stat_http as h \
		LEFT JOIN quotas as q on (q.uid = h.uid and q.name = 'bw_web') \
		LEFT JOIN membres as m on (m.uid = h.uid) \
		WHERE  IF (MONTH(NOW()) - $MONTH < 1, YEAR(day) = YEAR(NOW()) - 1, YEAR(day) = YEAR(NOW())) \
			AND IF (MONTH(NOW()) - $MONTH < 1, MONTH(day) = 12 - $MONTH + MONTH(NOW()), MONTH(day) = MONTH(NOW()) - $MONTH ) \
		GROUP BY h.uid \
		HAVING webusage > total \
		ORDER BY webusage desc "
