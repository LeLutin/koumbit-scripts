#! /usr/bin/perl -w

# allow users to override the SVNBASE
require "$ENV{HOME}/.process_pay.conf" if -f "$ENV{HOME}/.process_pay.conf";

# we expect this to set $YEAR (2010-2011) and $COMPANY (RP12345)
require "/etc/process_pay.conf" if -f "/etc/process_pay.conf";

while (($id, $emp) = each(%EMPLOYEES)) {
  print "Checkign key for $emp ($id)... ";
  if ($emp =~ m/devnull/) {
    print "skipped\n";
    next;
  }
  my @gpg = `echo | gpg -e -a --batch -r $emp 2>&1 > /dev/null`;
  if ($? == 0) {
    print "key ok!\n";
  } else {
    print "key bad: " . $gpg[0];
  }
}
